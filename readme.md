1. Create Database attendance_basic;
2. Edit .env file for database connection
3. Install Composer
4. Run Terminal / Command Line
5. Change Directory to attendance_basic/code
6. Run "composer_update"
7. Run "php artisan migrate --seed"
8. Default Login
    email: admin@gmail.com
    pass: admin123