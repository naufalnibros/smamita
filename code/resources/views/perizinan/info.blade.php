@extends('layouts.main')

@section('title')
Perizinan -
@endsection

@push('styles')

@endpush

@section('content')
<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
            <!--begin: Form Wizard Form-->
            <form class="kt-form" id="kt_user_add_form" action="{{ route("perizinan.tambah.simpan") }}" novalidate="novalidate" method="POST">
                <!--begin: Form Wizard Step 1-->
                <div class="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                    <div class="kt-heading kt-heading--md">Form Perizinan Keluar Kelas</div>
                    <div class="kt-section kt-section--first">
                        <div class="kt-wizard-v4__form">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="kt-section__body">
                                        @csrf
                                        <!-- Form -->
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Jam Keluar</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input type="time" class="form-control" name="jam_keluar" value="{{ date("H:i:s")  }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Jam Kembali</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input type="time" class="form-control" name="jam_kembali" value="{{ date("H:i:s")  }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Guru</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <select class="form-control" name="master_user_id">
                                                    @foreach($guru as $value)
                                                        <option value="{{ $value->master_user_id }}">{{ $value->nama  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Keterangan</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <textarea class="form-control" type="text" placeholder="Keterangan" name="keterangan"></textarea>
                                            </div>
                                        </div>
                                        <!-- Form -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn btn-success btn-block">
                        <button class="btn btn-success btn-block" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')

@endpush
