@extends('layouts.main')

@section('title')
Student -
@endsection

@push('styles')
<style>
    td {
        vertical-align: middle !important;
        line-height: 15px;
    }
</style>
@endpush

@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Perizinan Keluar Kelas</h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="{{ route("perizinan.tambah") }}" class="btn btn-outline-primary">Tambah Perizinan</a>
            </div>
        </div>
    </div>
</div>

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12 border-right border-right-dashed" style="min-height: 400px">
                <form action="" method="get">
                    @csrf
                    <div class="input-group">
                        <input type="text" class="form-control" name="keyword" id="search_keyword" placeholder="Keterangan....">
                        <div class="input-group-prepend">
                            <button class="btn btn-primary btn-sm btn-round-right"><i class="flaticon-search text-white"></i></button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="2%">#</th>
                                <th>NAMA SISWA</th>
                                <th>NAMA GURU</th>
                                <th>Jam Keluar</th>
                                <th>Jam Kembali</th>
                                <th>Keterangan</th>
                                <th>Status Disetujui</th>
                                <th>Status Kembali</th>
                                @if(session()->get("user")->type === "guru")
                                <th class="text-center">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($models) == 0)
                            <tr>
                                <td class="text-center" colspan="10">Not Found</td>
                            </tr>
                            @endif
                            @foreach($models as $key => $value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->siswa_nipd  }} - {{ $value->siswa_nama }}</td>
                                <td>{{ empty($value->guru_nama) ? "Data guru tidak ditemukan" : ($value->guru_nama) }}</td>
                                <td>{{ date("H:i:s", strtotime($value->jam_keluar)) }}</td>
                                <td>{{ date("H:i:s", strtotime($value->jam_kembali)) }}</td>
                                <td>{{ $value->keterangan  }}</td>
                                <td>{{ empty($value->approved_at) ? "Belum Disetujui" : "Disetujui pada : " . date("d-m-Y H:i:s", strtotime($value->approved_at))  }}</td>
                                <td>{{ empty($value->kembali_at) ? "Belum Konfirmasi Kembali" : "Konfirmasi Kembali pada : " . date("d-m-Y H:i:s", strtotime($value->kembali_at))  }}</td>
                                @if(session()->get("user")->type === "guru")
                                <td>
                                    <a class="btn btn-success btn-block" href="{{ route("perizinan.disetujui", $value->id)  }}">Disetujui</a>
                                    <a class="btn btn-warning btn-block" href="{{ route("perizinan.konfirmasi", $value->id)  }}">Konfirmasi Kembali</a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $models->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
