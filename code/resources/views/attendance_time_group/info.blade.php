@extends('layouts.main')

@section('title')
    Attendance Time Group -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($id == 'new') Add New @else Edit @endif Attendance Time Group</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Time Group</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">@if($id == 'new') Add New @else Edit Data @endif</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('attendance_time_group') }}" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-secondary">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('attendance_time_group.save', $id) }}" method="post">
                        @csrf
                        <div class="form-group pr-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="group">Group</label>
                            <input type="text" class="form-control" id="group" name="group" value="{{ old('group') }}">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group pr-3">
                                    <label for="date_start">Date Start</label>
                                    <input type="text" class="form-control datepicker" id="date_start" name="date_start" value="{{ old('date_start') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pr-3">
                                    <label for="date_end">Date End</label>
                                    <input type="text" class="form-control datepicker" id="date_end" name="date_end" value="{{ old('date_end') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right pr-3">
                            <button type="submit" class="btn btn-primary">Save Attendance Time Group</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($id != 'new')
        <script>
            document.getElementById('name').value = "{{ $field->name }}";
            document.getElementById('date_start').value = "{{ format_date($field->date_start) }}";
            document.getElementById('date_end').value = "{{ format_date($field->date_end) }}";
        </script>
    @endif
@endsection

@push('scripts')

@endpush
