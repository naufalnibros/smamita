@extends('layouts.main')

@section('title')
    Attendance Time Group -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Data Attendance Time Group</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Master</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Time Group</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('attendance_time_group.info', 'new') }}" class="btn btn-outline-primary">Add New Attendance Time Group</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6 border-right border-right-dashed" style="min-height: 400px">
                    <form action="{{ route('attendance_time_group.search') }}" method="post">
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="filter" id="search_filter" value="name">
                            <input type="text" class="form-control" name="keyword" id="search_keyword" placeholder="Search ...">
                            <div class="input-group-prepend">
                                <button class="btn btn-primary btn-sm btn-round-right"><i class="flaticon-search text-white"></i></button>
                            </div>
                        </div>
                        @include('tools.search')
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>Name</th>
                                <th class="text-center">Group</th>
                                <th class="text-right">Date Start</th>
                                <th class="text-left">Date End</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td class="text-center" colspan="10">Not Found</td>
                                </tr>
                            @endif
                            @foreach($data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td class="text-center">{{ $value->group }}</td>
                                    <td class="text-right">{{ format_date($value->date_start) }}</td>
                                    <td class="text-left">{{ format_date($value->date_end) }}</td>
                                    <td class="text-right">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-clean btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more-1"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <a href="{{ route('attendance_time_group.info', $value->id) }}" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                            <span class="kt-nav__link-text">Edit Data</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="javascript:void(0)" onclick="deleteData('formDelete{{ $value->id }}')" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-delete"></i>
                                                            <span class="kt-nav__link-text">Delete Data</span>
                                                        </a>
                                                        <form action="{{ route('attendance_time_group.delete', $value->id) }}" method="post" id="formDelete{{ $value->id }}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <a class="btn btn-clean btn-sm btn-icon ml-2" onclick="displayTime({{ $value }})">
                                            <i class="flaticon2-right-arrow"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->appends(request()->input())->links() }}
                </div>
                <div class="col-md-6 pl-4">
                    <div class="kt-notification-v2" id="attendanceTimeGroupDetail" style="display: none;">
                        @php($daysText = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'))
                        @for($i = 0 ; $i < 7; $i++)
                            <a href="javascript:void(0)" class="kt-notification-v2__item" onclick="displayAttendanceTime({{ $i }})">
                                <div class="kt-notification-v2__item-icon">
                                    <i class="flaticon-time kt-font-success"></i>
                                </div>
                                <div class="kt-notification-v2__itek-wrapper">
                                    <div class="kt-notification-v2__item-title">
                                        {{ $daysText[$i] }}
                                    </div>
                                    <div class="kt-notification-v2__item-desc" id="attendanceTime{{ $i+1 }}">
                                        -
                                    </div>
                                </div>
                            </a>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modals')
    <div class="modal fade" id="attendanceTimeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Attendance Time</h4>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-xs-height full-height">
                        <div class="form-group">
                            <label for="name">Day</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="flag_day_off">Is Day Off</label>
                            <select name="flag_day_off" id="flag_day_off" class="form-control select2">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="row row-time">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="time_in_start">Time In Start</label>
                                    <input type="text" class="form-control timepicker" id="time_in_start" name="time_in_start">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="time_in_end">Time In End</label>
                                    <input type="text" class="form-control timepicker" id="time_in_end" name="time_in_end">
                                </div>
                            </div>
                        </div>
                        <div class="row row-time">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="time_out_start">Time Out Start</label>
                                    <input type="text" class="form-control timepicker" id="time_out_start" name="time_out_start">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="time_out_end">Time Out End</label>
                                    <input type="text" class="form-control timepicker" id="time_out_end" name="time_out_end">
                                </div>
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="saveAttendanceTime()">Save Attendance Time</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('scripts')
    <script>
        let selectedGroupId = null, selectedDayIndex = null, selectedAttendanceTime = 'new';
        const dayText = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        function setDisplayAttendanceTime(index, data){
            if(data.flag_day_off != 1){
                $('#attendanceTime'+(index+1)).html('Time In : '+data.time_in_start+' - '+data.time_in_end+'' +
                    '<br>' +
                    'Time Out : '+data.time_out_start+' - '+data.time_out_end);
            }else{
                $('#attendanceTime'+(index+1)).html('Day Off');
            }
        }
        function displayTime(data){
            $('#attendanceTimeGroupDetail').show();
            selectedGroupId = data.id;
            $.get("{{ route('attendance_time') }}?mode=ajax&show=all&search=attendance_time_group_id="+data.id,function (result) {
                $.each(result, function (i, value) {
                    setDisplayAttendanceTime(i, value);
                });
            });
        }
        function displayAttendanceTime(index){
            if(selectedGroupId !== null){
                selectedDayIndex = index+1;
                $('#name').val(dayText[index]);
                $('#attendanceTimeModal').modal('show');
            }
        }
        function saveAttendanceTime(){
            $.post("{{ url('attendance_time/save') }}/"+selectedAttendanceTime+"?mode=ajax",{
                _token: "{{ csrf_token() }}",
                attendance_time_group_id: selectedGroupId,
                name: $('#name').val(),
                day: selectedDayIndex,
                flag_day_off: $('#flag_day_off').find('option:selected').val(),
                time_in_start: $('#time_in_start').val(),
                time_in_end: $('#time_in_end').val(),
                time_out_start: $('#time_out_start').val(),
                time_out_end: $('#time_out_end').val()
            },function (result) {
                setDisplayAttendanceTime(selectedDayIndex-1, result);
                $('#name').val('');
                $('#time_in_start').val('');
                $('#time_in_end').val('');
                $('#time_out_start').val('');
                $('#time_out_end').val('');
                $('#attendanceTimeModal').modal('toggle');
            }).fail(function (xhr) {
                console.log(xhr.responseText);
            });
        }

        $('#attendanceTimeModal').on("shown.bs.modal", function() {
            $('.select2').select2();
        });
        $('#flag_day_off').change(function () {
            let flag = $('#flag_day_off').find('option:selected').val();
            if(flag === '1'){
                $('.row-time').hide();
            }else{
                $('.row-time').show();
            }
        });
    </script>
@endpush
