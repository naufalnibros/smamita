@extends('layouts.main')

@section('title')
    Attendance -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
        .td-att-Lbr{
            background-color: #eaeaea!important;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Data Attendance</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">

                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <form action="{{ route('attendance.search') }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-2">
                        <input type="text" class="form-control datepicker-month" name="month" id="month" placeholder="Select Month">
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="search" id="search" placeholder="Search Name ...">
                    </div>
                    <div class="col-md-2 text-right">
                        <button type="submit" class="btn btn-block btn-primary">Search</button>
                    </div>
                </div>
                <script>
                    document.getElementById('month').value = "{{ $month }}";
                    document.getElementById('search').value = "{{ $search }}";
                </script>
            </form>
            <hr>
            @if(($month != null))
                @php
                    $dayIndonesia = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
                @endphp
                <div class="table-responsive">
                    <table class="table table-hover table-pill">
                        <thead>
                        <tr>
                            <th rowspan="2" width="3%">#</th>
                            <th rowspan="2">Name</th>
                            <th colspan="{{ $days_in_month }}" class="text-center">Date</th>
                        </tr>
                        <tr>
                            @for($i = 1; $i <= $days_in_month; $i++)
                                <th class="text-center">{{ $i }}<br>{{ $dayIndonesia[date('N', strtotime($i.'-'.$month))-1] }}</th>
                            @endfor
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($employee as $key => $value)
                            <tr>
                                <td>{{ (($employee->currentPage()-1)*10)+($key+1) }}</td>
                                <td class="text-nowrap">{{ $value->name }}<br><small>No.ID : {{ $value->no_id }}</small></td>
                                @for($i = 1; $i <= $days_in_month; $i++)
                                    <td style="cursor: pointer;" class="text-center td-att-{{ str_replace(['1','2','3','4','5'], "", $value->attendance[$i-1]['code']) }}" onclick="showAttendanceLog({{ json_encode($value->attendance_log[$i-1]) }}, {{ json_encode($value->attendance[$i-1]) }}, '{{ $value->nip }}', '{{ $value->nama_lengkap }}')">
                                        @if($value->attendance[$i-1]['time_in'] == null)
                                            -
                                        @else
                                            {{ $value->attendance[$i-1]['time_in'] }}
                                        @endif
                                        <br>
                                        @if($value->attendance[$i-1]['time_out'] == null)
                                            -
                                        @else
                                            {{ $value->attendance[$i-1]['time_out'] }}
                                        @endif
                                        <br>
                                        {{ $value->attendance[$i-1]['code'] }}
                                    </td>
                                @endfor
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="mt-4">
                    {{ $employee->appends(request()->input())->links() }}
                </div>
            @else
                <div class="card border-0 mt-4">
                    <div class="card-body text-center">
                        <h3>Select Month then press Search</h3>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')

@endpush
