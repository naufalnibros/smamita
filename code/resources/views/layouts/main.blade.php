<!DOCTYPE html>

<html lang="en" >

<head>
    <meta charset="utf-8"/>

    <title>@yield('title')SISTEM INFORMASI SISWA SMAMITA</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,400,500,600,700">
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/favicon.ico') }}" />
    <style>
        .text-black{
            color: #000!important;
        }
        .text-black-50{
            color: #636177!important;
        }
        .border-right-dashed{
            border-right-style: dashed!important;
        }
        .btn-round-right{
            border-top-right-radius: 4px!important;
            border-bottom-right-radius: 4px!important;
        }
        input.datepicker{
            width: 100%!important;
        }
        @media (min-width: 1025px){
            .kt-header--fixed .kt-wrapper {
                padding-top: 0;
            }
        }

    </style>
    @stack('styles')

</head>

<body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

<div id="kt_header_mobile" class="kt-header-mobile kt-header-mobile--fixed " >
    <div class="kt-header-mobile__logo">
        <a href="{{ url('admin') }}">
            <img alt="Logo" src="{{ asset('assets/media/logos/logo-6.png') }}"/>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
    </div>
</div>

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

        <div class="kt-aside-menu-overlay"></div>

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <div id="kt_header" class="kt-header kt-grid kt-grid--ver  " >
                <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
                    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout- "  >
                        <ul class="kt-menu__nav ">

                            <li class="kt-menu__item"><a href="{{ route("home.index")  }}" class="kt-menu__link "><span class="kt-menu__link-text">DASHBOARD</span></a></li>

                            @if (session()->get("user")->type === "siswa" || session()->get("user")->type === "walimurid")
                                <li class="kt-menu__item"><a href="{{ route("absensi")  }}" class="kt-menu__link "><span class="kt-menu__link-text">ABSENSI SISWA</span></a></li>
                            @endif

                            <li class="kt-menu__item"><a href="{{ route("perizinan")  }}" class="kt-menu__link "><span class="kt-menu__link-text">PERIZINAN KELUAR KELAS</span></a></li>

                            <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                <a href="javascript:void(0)" class="kt-menu__link kt-menu__toggle">
                                    <span class="kt-menu__link-text">INFORMASI</span>
                                    <i class="kt-menu__hor-arrow la la-angle-down"></i>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                </a>
                                <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item " aria-haspopup="true">
                                            <a href="{{ route("prestasi") }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                <span class="kt-menu__link-text">PRESTASI</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item " aria-haspopup="true">
                                            <a href="{{ route("pelanggaran") }}" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                <span class="kt-menu__link-text">PELANGGARAN</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item " aria-haspopup="true">
                                            <a href="" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                <span class="kt-menu__link-text">JADWAL</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item " aria-haspopup="true">
                                            <a href="" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                <span class="kt-menu__link-text">INFORMASI SEKOLAH</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="kt-header__topbar">
                    <div class="kt-header__topbar-item dropdown">
                        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                            <span class="kt-header__topbar-icon kt-header__topbar-icon--success"><i class="flaticon2-bell-alarm-symbol"></i></span>
                            <span class="kt-hidden kt-badge kt-badge--danger"></span>
                        </div>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                            <div class="kt-head kt-head--skin-light kt-head--fit-x kt-head--fit-b">
                                <h3 class="kt-head__title">
                                    User Notifications
                                </h3>
                                <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-line-chart kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New order has been received
                                            </div>
                                            <div class="kt-notification__item-time">
                                                2 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-box-1 kt-font-brand"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                New customer is registered
                                            </div>
                                            <div class="kt-notification__item-time">
                                                3 hrs ago
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-header__topbar-item kt-header__topbar-item--user">
                        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                            <span class=" kt-header__topbar-welcome">Hi,</span>
                            <span class=" kt-header__topbar-username text-black-50">{{ session('user')->nama }}</span>
                            <span class="kt-header__topbar-icon"><i class="flaticon2-user-outline-symbol"></i></span>
                        </div>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                            <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                                <div class="kt-user-card__avatar">
                                    <img class="kt-badge" alt="Pic" src="{{ asset('images/user_circle.png') }}" />
                                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                                </div>
                                <div class="kt-user-card__name">
                                    {{ session('user')->nama }}
                                </div>
                            </div>

                            <div class="kt-notification">
                                <div class="kt-notification__custom kt-space-between">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                <div class="kt-container kt-container--fluid kt-grid__item kt-grid__item--fluid">
                    @yield('content')
                </div>
            </div>
            <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" >
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        {{ date('Y') }}&nbsp;&copy;&nbsp;<a href="" target="_blank" class="kt-link">SMAMITA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stack('modals')


<script>
    var KTAppOptions = {"colors":{"state":{"brand":"#22b9ff","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
</script>
<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
<script>
    function deleteData(target){
        Swal.fire({
            title: 'Anda yakin?',
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $('#'+target).submit();
            }
        });
    }
    function format_date(nStr, separator = '-'){
        let dateArray = nStr.split('-');
        return dateArray[2] + separator + dateArray[1] + separator + dateArray[0];
    }
    function add_commas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    function remove_commas(nStr){
        nStr = nStr.replace(/\./g,'');
        return nStr;
    }
    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
        $('.xdsoft_autocomplete_dropdown').css( "overflow", "inherit!important" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
        $('.xdsoft_autocomplete_dropdown').css( "overflow", "auto!important" );
    });
    $('.select2').select2();
    $('.kt-selectpicker').selectpicker();
    $('.datepicker').datepicker({
        autoclose:true,
        format:'dd-mm-yyyy',
        oritentation:"auto",
        useCurrent: false,
    });
    $('.datepicker-month').datepicker({
        autoclose: true,
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months",
    });
    $('.timepicker').timepicker({
        showMeridian: false,
        showSeconds: true,
        icons: {
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down'
        }
    });
</script>
@stack('scripts')
</body>
</html>
