@extends('layouts.main')

@section('title')
    User Level -
@endsection

@push('styles')
    <link href="{{ asset('assets/plugins/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($id == 'new') Add New @else Edit @endif User Level</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">User Level</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">@if($id == 'new') Add New @else Edit Data @endif</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('user_level') }}" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6 border-right border-right-dashed" style="min-height: 400px">
                    <form action="{{ route('user_level.save', $id) }}" method="post">
                        @csrf
                        <input type="hidden" name="selected_credentials" id="selected_credentials">
                        <div class="form-group pr-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" rows="4" class="form-control">{{ old('description') }}</textarea>
                        </div>
                        <div class="form-group text-right pr-3">
                            <button type="submit" class="btn btn-primary">Save User Level</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <div id="treeViewCredentials" class="tree-demo"></div>
                </div>
            </div>
        </div>
    </div>

    @if($id != 'new')
        <script>
            document.getElementById('name').value = "{{ $field->name }}";
            document.getElementById('description').value = "{{ $field->description }}";
            document.getElementById('selected_credentials').value = "{{ $field->selected_credentials }}";
        </script>
    @endif
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/custom/jstree/jstree.bundle.js') }}" type="text/javascript"></script>
    <script>
        let selectedFeature = [];
        let credentials = JSON.parse(`{!! json_encode($credentials) !!}`);
        $('#treeViewCredentials').jstree({
            core: {
                themes: {
                    responsive: 1
                }, data: credentials,
            },
            checkbox: {
                three_state : false,
                whole_node : false,
                tie_selection : false
            },
            types: {
                default: {
                    icon: "fa fa-circle"
                },
            },
            plugins: ["types", "checkbox"],
        }).bind("loaded.jstree", function (event, data) {
            $(this).jstree("open_all");
        }).on("check_node.jstree uncheck_node.jstree", function(e, data) {
            if(data.node.state.checked){
                selectedFeature.push(data.node.original.text);
            }else{
                let index = selectedFeature.indexOf(data.node.original.text);
                if (index !== -1) selectedFeature.splice(index, 1);
            }
            $('#selected_credentials').val(selectedFeature.toString());
        })

    </script>
@endpush
