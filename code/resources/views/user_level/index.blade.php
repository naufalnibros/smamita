@extends('layouts.main')

@section('title')
    User Level -
@endsection

@push('styles')
    <link href="{{ asset('assets/plugins/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Data User Level</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">User Level</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('user_level.info', 'new') }}" class="btn btn-outline-primary">Add New User Level</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6 border-right border-right-dashed" style="min-height: 400px">
                    <form action="{{ route('user_level.search') }}" method="post">
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="filter" id="search_filter" value="name">
                            <input type="text" class="form-control" name="keyword" id="search_keyword" placeholder="Search ...">
                            <div class="input-group-prepend">
                                <button class="btn btn-primary btn-sm btn-round-right"><i class="flaticon-search text-white"></i></button>
                            </div>
                        </div>
                        @include('tools.search')
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td class="text-center" colspan="3">Not Found</td>
                                </tr>
                            @endif
                            @foreach($data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td class="text-right">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-clean btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more-1"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <a href="{{ route('user_level.info', $value->id) }}" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                            <span class="kt-nav__link-text">Edit Data</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="javascript:void(0)" onclick="deleteData('formDelete{{ $value->id }}')" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-delete"></i>
                                                            <span class="kt-nav__link-text">Delete Data</span>
                                                        </a>
                                                        <form action="{{ route('user_level.delete', $value->id) }}" method="post" id="formDelete{{ $value->id }}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <a class="btn btn-clean btn-sm btn-icon ml-2" onclick="displayTreeView({{ $value }})">
                                            <i class="flaticon2-right-arrow"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->appends(request()->input())->links() }}
                </div>
                <div class="col-md-6 pl-3">
                    <h5>Credentials</h5>
                    <div id="treeViewCredentials" class="tree-demo">-</div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/custom/jstree/jstree.bundle.js') }}" type="text/javascript"></script>
    <script>
        function displayTreeView(data){
            $.get("{{ url('user_level/credentials') }}/"+data.id, function (result) {
                $('#treeViewCredentials').jstree({
                    core: {
                        themes: {
                            responsive: 1
                        }, data: result,
                    },
                    types: {
                        default: {
                            icon: "fa fa-circle"
                        },
                    },
                    plugins: ["types"],
                }).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
                $('#treeViewCredentials').jstree(true).settings.core.data = result;
                $('#treeViewCredentials').jstree(true).refresh();
                $("#treeViewCredentials").jstree("open_all");
            });
        }
    </script>
@endpush
