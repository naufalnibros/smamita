@extends('layouts.main')

@section('title')
    Attendance Absence -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($id == 'new') Add New @else Edit @endif Attendance Absence</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Absence</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">@if($id == 'new') Add New @else Edit Data @endif</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('attendance_absence') }}" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-secondary">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('attendance_absence.save', $id) }}" method="post">
                        @csrf
                        <input type="hidden" name="student_id" id="student_id" value="{{ old('student_id') }}">
                        <input type="hidden" name="student_data" id="student_data">
                        <div class="form-group pr-3">
                            <label for="student_no_id">Student No.ID</label>
                            <input type="text" class="form-control" id="student_no_id" name="student_no_id" value="{{ old('student_no_id') }}" onclick="searchStudent()" placeholder="Click to Search" readonly>
                        </div>
                        <div class="form-group pr-3">
                            <label for="student_name">Student Name</label>
                            <input type="text" class="form-control" id="student_name" name="student_name" value="{{ old('student_name') }}" onclick="searchStudent()" placeholder="Click to Search"  readonly>
                        </div>
                        <div class="form-group pr-3">
                            <label for="code">Code</label>
                            <select name="code" id="code" class="form-control select2">
                                @foreach($code_array as $item)
                                    <option value="{{ $item['id'] }}">{{ $item['caption'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group pr-3">
                            <label for="date_start">Date Start</label>
                            <input type="text" class="form-control datepicker" id="date_start" name="date_start" value="{{ old('date_start') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="date_end">Date End</label>
                            <input type="text" class="form-control datepicker" id="date_end" name="date_end" value="{{ old('date_end') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" id="description" name="description" value="{{ old('description') }}" >
                        </div>
                        <div class="form-group text-right pr-3">
                            <button type="submit" class="btn btn-primary">Save Attendance Absence</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($id != 'new')
        <script>
            document.getElementById('student_id').value = "{{ $field->student_id }}";
            document.getElementById('student_data').value = `{!! json_encode($field->student) !!}`;
            document.getElementById('code').value = "{{ $field->code }}";
            document.getElementById('date_start').value = "{{ format_date($field->date_start) }}";
            document.getElementById('date_end').value = "{{ format_date($field->date_end) }}";
            document.getElementById('description').value = "{{ $field->description }}";
        </script>
    @endif
@endsection

@include('modals.student')

@push('scripts')
    <script>
        function searchStudent(){
            openStudentModal('student_id', 'student_data');
        }
        $('#student_id').change(function () {
            let data = $('#student_data').val();
            if(data !== ''){
                data = JSON.parse(data);

                $('#student_no_id').val(data.no_id);
                $('#student_name').val(data.name);
            }
        });
        $('#student_id').trigger('change');
    </script>
@endpush
