@extends('layouts.main')

@section('title')
    User -
@endsection

@push('styles')
    <link href="{{ asset('assets/plugins/custom/jstree/jstree.bundle.css') }}" rel="stylesheet" type="text/css" />
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($id == 'new') Add New @else Edit @endif User</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">User</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">@if($id == 'new') Add New @else Edit Data @endif</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('user') }}" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6 border-right border-right-dashed" style="min-height: 400px">
                    @if ($errors->any())
                        <div class="alert alert-secondary">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('user.save', $id) }}" method="post">
                        @csrf
                        <div class="form-group pr-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group pr-3">
                            <label for="password_confirmation">Repeat Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                        </div>
                        <div class="form-group pr-3">
                            <label for="user_level_id">User Level</label>
                            <select name="user_level_id" id="user_level_id" class="form-control select2">
                                @foreach($user_level as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($id != 'new')
                            <i>* Empty Password if no changes</i>
                        @endif
                        <div class="form-group text-right pr-3">
                            <button type="submit" class="btn btn-primary">Save User</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    User Level Credentials
                    <div id="treeViewCredentials" class="tree-demo">-</div>
                </div>
            </div>
        </div>
    </div>

    @if($id != 'new')
        <script>
            document.getElementById('name').value = "{{ $field->name }}";
            document.getElementById('email').value = "{{ $field->email }}";
            document.getElementById('user_level_id').value = "{{ $field->user_level_id }}";
        </script>
    @endif
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/custom/jstree/jstree.bundle.js') }}" type="text/javascript"></script>
    <script>
        function displayTreeView(id){
            $.get("{{ url('user_level/credentials') }}/"+id, function (result) {
                $('#treeViewCredentials').jstree({
                    core: {
                        themes: {
                            responsive: 1
                        }, data: result,
                    },
                    types: {
                        default: {
                            icon: "fa fa-circle"
                        },
                    },
                    plugins: ["types"],
                }).bind("loaded.jstree", function (event, data) {
                    $(this).jstree("open_all");
                });
                $('#treeViewCredentials').jstree(true).settings.core.data = result;
                $('#treeViewCredentials').jstree(true).refresh();
                $("#treeViewCredentials").jstree("open_all");
            });
        }
        $('#user_level_id').change(function () {
            let id = $('#user_level_id').find('option:selected').val();
            displayTreeView(id);
        });
        $('#user_level_id').trigger('change');
    </script>
@endpush
