@extends('layouts.main')

@section('title')
    User Log -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Data User Log</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">User Log</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6 border-right border-right-dashed" style="min-height: 400px">
                    <form action="{{ route('user_log.search') }}" method="post">
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="filter" id="search_filter" value="name">
                            <input type="text" class="form-control" name="keyword" id="search_keyword" placeholder="Search ...">
                            <div class="input-group-prepend">
                                <button class="btn btn-primary btn-sm btn-round-right"><i class="flaticon-search text-white"></i></button>
                            </div>
                        </div>
                        @include('tools.search')
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>User Log Level</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td class="text-center" colspan="3">Not Found</td>
                                </tr>
                            @endif
                            @foreach($data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->user_level->name }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-clean btn-sm btn-icon ml-2" href="{{ route('user_log', ['search='.$search, 'user_id='.$value->id]) }}">
                                            <i class="flaticon2-right-arrow"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->appends(request()->input())->links() }}
                </div>
                <div class="col-md-6 pl-3">
                    @if($user_id != null)
                        <form action="{{ route('user_log.search') }}" method="post">
                            @csrf
                            <div class="input-group">
                                <input type="hidden" name="user_id" value="{{ $user_id }}">
                                <input type="text" class="form-control datepicker" name="date" id="date" value="{{ date('d-m-Y', strtotime($date)) }}">
                                <div class="input-group-prepend">
                                    <button class="btn btn-primary btn-sm btn-round-right"><i class="flaticon-search text-white"></i></button>
                                </div>
                            </div>
                            @include('tools.search')
                        </form>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th>Method</th>
                                    <th>Path</th>
                                    <th>Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($data) == 0)
                                    <tr>
                                        <td class="text-center" colspan="3">Not Log Found</td>
                                    </tr>
                                @endif
                                @foreach($log_data as $key => $value)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->method }}</td>
                                        <td>{{ $value->path }}</td>
                                        <td>{{ $value->request_data }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

    </script>
@endpush
