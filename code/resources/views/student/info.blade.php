@extends('layouts.main')

@section('title')
    Student -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($id == 'new') Add New @else Edit @endif Student</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Student</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">@if($id == 'new') Add New @else Edit Data @endif</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('student') }}" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-secondary">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('student.save', $id) }}" method="post">
                        @csrf
                        <div class="form-group pr-3">
                            <label for="no_id">No.ID</label>
                            <input type="text" class="form-control" id="no_id" name="no_id" value="{{ old('no_id') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="no_attendance">No.Attendance</label>
                            <input type="text" class="form-control" id="no_attendance" name="no_attendance" value="{{ old('no_attendance') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="class">Class</label>
                            <input type="text" class="form-control" id="class" name="class" value="{{ old('class') }}">
                        </div>
                        <div class="form-group text-right pr-3">
                            <button type="submit" class="btn btn-primary">Save Student</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($id != 'new')
        <script>
            document.getElementById('no_id').value = "{{ $field->no_id }}";
            document.getElementById('name').value = "{{ $field->name }}";
            document.getElementById('no_attendance').value = "{{ $field->no_attendance }}";
            document.getElementById('class').value = "{{ $field->class }}";
        </script>
    @endif
@endsection

@push('scripts')

@endpush
