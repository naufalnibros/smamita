@extends('layouts.auth')

@section('content')
    <div class="kt-login__signin" style="
    background: #b5bcfff0;
    padding-top: 1em;
    padding-bottom: 1em;">
        <div class="kt-login__head">
            <h3 class="kt-login__title">LOGIN SMAMITA</h3>
        </div>
        <form class="kt-form" action="{{ route('proses') }}" method="post" style="margin: 2em;">
            @csrf
            <div class="input-group" style="width: fit-content;margin: 1em;">
                <input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off">
            </div>
            @if(!empty($message["username"]))
                <i>*{{ $message["username"] }}</i>
            @endif
            <div class="input-group" style="width: fit-content;margin: 1em;">
                <input class="form-control" type="password" placeholder="Password" name="password">
            </div>
            @if(!empty($message["password"]))
            <i>*{{ $message["password"] }}</i>
            @endif
            <div class="row kt-login__extra">
                <div class="col">
                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                        <input type="checkbox" name="remember"> Remember me
                        <span></span>
                    </label>
                </div>
                <div class="col kt-align-right">

                </div>
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>
            </div>
        </form>
    </div>
@endsection
