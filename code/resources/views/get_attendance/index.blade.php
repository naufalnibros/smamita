@extends('layouts.main')

@section('title')
    Get Attendance -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Data Get Attendance</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Master</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Get Attendance</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 border-right border-right-dashed" style="min-height: 400px">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>Name</th>
                                <th class="text-right">IP Address</th>
                                <th class="text-left">Port</th>
                                <th class="text-left">Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td class="text-center" colspan="10">Not Found</td>
                                </tr>
                            @endif
                            @foreach($data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td class="text-right">{{ $value->ip_address }}</td>
                                    <td class="text-left">{{ $value->port }}</td>
                                    <td class="text-left">{{ $value->status }}</td>
                                    <td class="text-right p-1">
                                        <a href="{{ route('get_attendance.machine', $value->id) }}" class="btn btn-sm btn-success">Get Log</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
