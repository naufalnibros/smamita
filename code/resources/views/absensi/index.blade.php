@extends('layouts.main')

@section('title')
    Absensi -
@endsection

@push('styles')

@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Absensi</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Data Absensi Siswa</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            {{--      Content      --}}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-blank">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered table-print">
                                    <thead>
                                    <tr>
                                        <th style="vertical-align : middle;text-align:center;" rowspan="2">Tanggal</th>
                                        <th style="vertical-align : middle;text-align:center;" colspan="2">Jam Absensi</th>
                                        <th style="vertical-align : middle;text-align:center;" rowspan="2">Keterangan</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: center;">
                                            Jam Masuk <br>
                                            05:00 - 07:00
                                        </th>
                                        <th style="text-align: center;">
                                            Jam Pulang <br>
                                            15:30 - 17:00
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($models as $data)
                                        <tr>
                                            <td class="text-center">{{ $data["tanggal"]  }}</td>
                                            <td class="text-center">{{ $data["jam_masuk"] }}</td>
                                            <td class="text-center">{{ $data["jam_pulang"] }}</td>
                                            <td class="text-center">{{ $data["keterangan"] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--      Content      --}}
        </div>
    </div>

@endsection

@push('scripts')

@endpush
