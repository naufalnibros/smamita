@extends('layouts.main')

@section('title')
    Attendance Machine -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">@if($id == 'new') Add New @else Edit @endif Attendance Machine</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Machine</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">@if($id == 'new') Add New @else Edit Data @endif</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('attendance_machine') }}" class="btn btn-outline-primary">Back</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-secondary">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text">
                                <ul class="mb-0">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('attendance_machine.save', $id) }}" method="post">
                        @csrf
                        <div class="form-group pr-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="ip_address">IP Address</label>
                            <input type="text" class="form-control" id="ip_address" name="ip_address" value="{{ old('ip_address') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="port">Port</label>
                            <input type="text" class="form-control" id="port" name="port" value="{{ old('port') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="key">Key</label>
                            <input type="text" class="form-control" id="key" name="key" value="{{ old('key') }}">
                        </div>
                        <div class="form-group pr-3">
                            <label for="flag_active">Active Status</label>
                            <select name="flag_active" id="flag_active" class="form-control kt-selectpicker">
                                <option value="1">Active</option>
                                <option value="0">Not Active</option>
                            </select>
                        </div>
                        <div class="form-group text-right pr-3">
                            <button type="submit" class="btn btn-primary">Save Attendance Machine</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($id != 'new')
        <script>
            document.getElementById('name').value = "{{ $field->name }}";
            document.getElementById('ip_address').value = "{{ $field->ip_address }}";
            document.getElementById('port').value = "{{ $field->port }}";
            document.getElementById('key').value = "{{ $field->key }}";
            document.getElementById('flag_active').value = "{{ $field->flag_active }}";
        </script>
    @endif
@endsection

@push('scripts')

@endpush
