@extends('layouts.main')

@section('title')
    Attendance Machine -
@endsection

@push('styles')
    <style>
        td{
            vertical-align: middle!important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Data Attendance Machine</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Master</a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a class="kt-subheader__breadcrumbs-link">Attendance Machine</a>
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('attendance_machine.info', 'new') }}" class="btn btn-outline-primary">Add New Attendance Machine</a>
                    <a href="{{ route('attendance_machine', ['page='.$data->currentPage(), 'check_status=true']) }}" class="btn btn-outline-primary">Check Status</a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 border-right border-right-dashed" style="min-height: 400px">
                    <form action="{{ route('attendance_machine.search') }}" method="post">
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="filter" id="search_filter" value="name">
                            <input type="text" class="form-control" name="keyword" id="search_keyword" placeholder="Search ...">
                            <div class="input-group-prepend">
                                <button class="btn btn-primary btn-sm btn-round-right"><i class="flaticon-search text-white"></i></button>
                            </div>
                        </div>
                        @include('tools.search')
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>Name</th>
                                <th class="text-right">IP Address</th>
                                <th class="text-left">Port</th>
                                @if($check_status != null)
                                    <th class="text-left">Status</th>
                                @endif
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data) == 0)
                                <tr>
                                    <td class="text-center" colspan="10">Not Found</td>
                                </tr>
                            @endif
                            @foreach($data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td class="text-right">{{ $value->ip_address }}</td>
                                    <td class="text-left">{{ $value->port }}</td>
                                    @if($check_status != null)
                                        <td class="text-left">{{ $value->status }}</td>
                                    @endif
                                    <td class="text-right">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-clean btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="flaticon-more-1"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <a href="{{ route('attendance_machine.info', $value->id) }}" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                            <span class="kt-nav__link-text">Edit Data</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-nav__item">
                                                        <a href="javascript:void(0)" onclick="deleteData('formDelete{{ $value->id }}')" class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon2-delete"></i>
                                                            <span class="kt-nav__link-text">Delete Data</span>
                                                        </a>
                                                        <form action="{{ route('attendance_machine.delete', $value->id) }}" method="post" id="formDelete{{ $value->id }}">
                                                            @csrf
                                                            @method('delete')
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <a class="btn btn-clean btn-sm btn-icon ml-2" onclick="displayTreeView({{ $value }})">
                                            <i class="flaticon2-right-arrow"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
