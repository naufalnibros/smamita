@extends('layouts.auth')

@section('content')
    <div class="kt-login__logo">
        <a href="#">

        </a>
    </div>
    <div class="kt-login__signin">
        <div class="kt-login__head">
            <h3 class="kt-login__title">Sign In To Access Attendance Application</h3>
        </div>
        <form class="kt-form" action="{{ route('login') }}" method="post">
            @csrf
            <div class="input-group">
                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
            </div>
            @error('email')
            <i>*{{ $message }}</i>
            @enderror
            <div class="input-group">
                <input class="form-control" type="password" placeholder="Password" name="password">
            </div>
            @error('password')
            <i>*{{ $message }}</i>
            @enderror
            <div class="row kt-login__extra">
                <div class="col">
                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                        <input type="checkbox" name="remember"> Remember me
                        <span></span>
                    </label>
                </div>
                <div class="col kt-align-right">

                </div>
            </div>
            <div class="kt-login__actions">
                <button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>
            </div>
        </form>
    </div>
@endsection
