@if($search != null)
    @php($search_part = explode(";", $search))
    @php(array_pop($search_part))
    @foreach($search_part as $item)
        @php($part = explode(",", $item))
        @if($part[1] == "like")
            @if((strpos($part[0], 'date') !== false))
                <script>
                    document.getElementById('search_{{ $part[0] }}').value = "{{ $part[2] }}";
                </script>
            @else
                <script>
                    document.getElementById('search_filter').value = "{{ $part[0] }}";
                    document.getElementById('search_keyword').value = "{{ $part[2] }}";
                </script>
            @endif
        @else
            <script>
                document.getElementById('search_{{ str_replace(".", "_", $part[0]) }}').value = "{{ $part[2] }}";
            </script>
        @endif
    @endforeach
@endif
