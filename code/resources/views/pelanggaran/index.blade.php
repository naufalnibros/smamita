@extends('layouts.main')

@section('title')
    Prestasi -
@endsection

@push('styles')
    <style>
        td {
            vertical-align: middle !important;
            line-height: 15px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Prestasi Siswa</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a class="kt-subheader__breadcrumbs-home"><i class="flaticon-earth-globe"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-12 border-right border-right-dashed" style="min-height: 400px">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="2%">#</th>
                                <th>NAMA SISWA</th>
                                <th>Jenis Prestasi</th>
                                <th>Keterangan</th>
                                <th>Dibuat Oleh</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($models) == 0)
                                <tr>
                                    <td class="text-center" colspan="10">Not Found</td>
                                </tr>
                            @endif
                            @foreach($models as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->siswa_nipd  }} - {{ $value->siswa_nama }}</td>
                                    <td>{{ $value->jenis }}</td>
                                    <td>{{ $value->keterangan }}</td>
                                    <td>{{ $value->user_nama }}</td>
                                    <td>
                                        @if(!empty($value->attachment))
                                            <a class="btn btn-success btn-block" href="">Attachment</a>
                                        @else
                                            <a class="btn btn-success btn-block" style="color: white">Tidak Ada Attachment</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{-- {{ $models->appends(request()->input())->links() }}--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
