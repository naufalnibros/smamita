@push('modals')
    <div class="modal fade" id="studentModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Student Search</h4>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container-xs-height full-height">
                        <div class="row m-2">
                            <div class="col-12">
                                <input type="text" class="form-control" id="student_search_modal" onkeyup="loadStudent()" placeholder="Search ...">
                                <table class="table table-hover mt-3">
                                    <thead>
                                    <tr>
                                        <th>No.ID</th>
                                        <th>Name</th>
                                        <th>Class</th>
                                    </tr>
                                    </thead>
                                    <tbody id="listStudent">
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 justify-content-center text-center mt-4">
                                <a href="javascript:void(0)" id="studentButtonBefore" class="btn btn-light float-left">Prev</a>
                                <a href="javascript:void(0)" id="studentButtonNext" class="btn btn-light float-right">Next</a>
                                <p id="studentPagePosition">0</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('scripts')
    <script>
        let targetInputStudent;
        let targetDataStudent;
        let dataStudentModal;
        let studentAdditionalFilter;

        function openStudentModal(target, target_data, additional_filter = ''){
            targetInputStudent = target;
            targetDataStudent = target_data;
            studentAdditionalFilter = additional_filter;
            loadStudent();
            $('#studentModal').modal('show');
        }
        function selectStudentModal(index){
            data = dataStudentModal[index];
            $('#'+targetInputStudent).val(data.id);
            $('#'+targetDataStudent).val(JSON.stringify(data));
            $('#'+targetInputStudent).trigger('change');
            $('#studentModal').modal('hide');
        }
        function loadStudent(pageUrl = "null"){
            let searchStudentModal = $('#student_search_modal').val();
            if(pageUrl === "null"){
                let searchUrl = "";
                pageUrl = "{{ url('student') }}?mode=ajax";
                if(searchStudentModal !== ''){
                    searchUrl = searchUrl + 'search=name,like,'+searchStudentModal+';';
                }
                if(studentAdditionalFilter !== ''){
                    searchUrl = (searchUrl === '') ? 'search=' : '&';
                    searchUrl = searchUrl + studentAdditionalFilter;
                }
                if(searchUrl !== ''){
                    searchUrl = '&'+searchUrl;
                }
                pageUrl = pageUrl + searchUrl;
            }
            $.get(pageUrl, function (result) {
                dataStudentModal = result.data;
                $('#studentPagePosition').html(result.current_page+" / "+result.last_page);
                $('#studentButtonBefore').attr("onclick","loadStudent('"+result.prev_page_url+"');");
                $('#studentButtonNext').attr("onclick","loadStudent('"+result.next_page_url+"');");

                $listStudent = $('#listStudent');
                $listStudent.empty();
                for(let i = 0; i < result.data.length; i++){
                    let row = $('<tr onclick="selectStudentModal('+i+')" style="cursor: pointer;">');
                    row.append('<td style="vertical-align: middle;">'+result.data[i].no_id+'</td>');
                    row.append('<td style="vertical-align: middle;">'+result.data[i].name+'</td>');
                    row.append('<td style="vertical-align: middle;">'+result.data[i].class+'</td>');
                    $listStudent.append(row);
                }
            });
        }
    </script>
@endpush
