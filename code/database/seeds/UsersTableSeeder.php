<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = \App\UserLevel::create([
            'name' => 'Super Admin',
            'description' => 'Full access',
            'credentials' => null
        ]);
        $user = \App\User::create([
            'user_level_id' => $level->id,
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
        ]);
        $user->password = \Illuminate\Support\Facades\Hash::make('admin123');
        $user->save();
    }
}
