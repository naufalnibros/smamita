<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('attendance_time_group_id');
            $table->string('name');
            $table->integer('day');
            $table->integer('flag_day_off');
            $table->time('time_in_start')->nullable();
            $table->time('time_in_end')->nullable();
            $table->time('time_out_start')->nullable();
            $table->time('time_out_end')->nullable();
            $table->timestamps();

            $table->foreign('attendance_time_group_id')->references('id')->on('attendance_time_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_times');
    }
}
