<?php

Route::get('storage/{folder}/{filename}', function ($folder,$filename){
    $path = storage_path('app/' . $folder . '/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('/', function () {
    return redirect('home');
});

Auth::routes(['register' => false, 'forgot' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user_level', 'UserLevelController@index')->name('user_level');
Route::post('/user_level/search', 'UserLevelController@search')->name('user_level.search');
Route::get('/user_level/info/{id}', 'UserLevelController@info')->name('user_level.info');
Route::post('/user_level/save/{id}', 'UserLevelController@save')->name('user_level.save');
Route::delete('/user_level/delete/{id}', 'UserLevelController@delete')->name('user_level.delete');
Route::get('/user_level/credentials/{id}', 'UserLevelController@credentials')->name('user_level.credentials');

Route::get('/user', 'UserController@index')->name('user');
Route::post('/user/search', 'UserController@search')->name('user.search');
Route::get('/user/info/{id}', 'UserController@info')->name('user.info');
Route::post('/user/save/{id}', 'UserController@save')->name('user.save');
Route::delete('/user/delete/{id}', 'UserController@delete')->name('user.delete');

Route::get('/user_log', 'UserLogController@index')->name('user_log');
Route::post('/user_log/search', 'UserLogController@search')->name('user_log.search');

Route::get('holiday','HolidayController@index')->name('holiday');
Route::post('holiday/search','HolidayController@search')->name('holiday.search');
Route::get('holiday/info/{id}','HolidayController@info')->name('holiday.info');
Route::post('holiday/save/{id}','HolidayController@save')->name('holiday.save');
Route::delete('holiday/delete/{id}','HolidayController@delete')->name('holiday.delete');

Route::get('attendance_machine','AttendanceMachineController@index')->name('attendance_machine');
Route::post('attendance_machine/search','AttendanceMachineController@search')->name('attendance_machine.search');
Route::get('attendance_machine/info/{id}','AttendanceMachineController@info')->name('attendance_machine.info');
Route::post('attendance_machine/save/{id}','AttendanceMachineController@save')->name('attendance_machine.save');
Route::delete('attendance_machine/delete/{id}','AttendanceMachineController@delete')->name('attendance_machine.delete');

Route::get('attendance_time_group','AttendanceTimeGroupController@index')->name('attendance_time_group');
Route::post('attendance_time_group/search','AttendanceTimeGroupController@search')->name('attendance_time_group.search');
Route::get('attendance_time_group/info/{id}','AttendanceTimeGroupController@info')->name('attendance_time_group.info');
Route::post('attendance_time_group/save/{id}','AttendanceTimeGroupController@save')->name('attendance_time_group.save');
Route::delete('attendance_time_group/delete/{id}','AttendanceTimeGroupController@delete')->name('attendance_time_group.delete');

Route::get('attendance_time','AttendanceTimeController@index')->name('attendance_time');
Route::post('attendance_time/search','AttendanceTimeController@search')->name('attendance_time.search');
Route::get('attendance_time/info/{id}','AttendanceTimeController@info')->name('attendance_time.info');
Route::post('attendance_time/save/{id}','AttendanceTimeController@save')->name('attendance_time.save');
Route::delete('attendance_time/delete/{id}','AttendanceTimeController@delete')->name('attendance_time.delete');

Route::get('student','StudentController@index')->name('student');
Route::post('student/search','StudentController@search')->name('student.search');
Route::get('student/info/{id}','StudentController@info')->name('student.info');
Route::post('student/save/{id}','StudentController@save')->name('student.save');
Route::delete('student/delete/{id}','StudentController@delete')->name('student.delete');

Route::get('attendance','AttendanceController@index')->name('attendance');
Route::post('attendance/search','AttendanceController@search')->name('attendance.search');

Route::get('attendance_absence','AttendanceAbsenceController@index')->name('attendance_absence');
Route::post('attendance_absence/search','AttendanceAbsenceController@search')->name('attendance_absence.search');
Route::get('attendance_absence/info/{id}','AttendanceAbsenceController@info')->name('attendance_absence.info');
Route::post('attendance_absence/save/{id}','AttendanceAbsenceController@save')->name('attendance_absence.save');
Route::delete('attendance_absence/delete/{id}','AttendanceAbsenceController@delete')->name('attendance_absence.delete');

Route::get('get_attendance','GetAttendanceController@index')->name('get_attendance');
Route::post('get_attendance.search','GetAttendanceController@search')->name('get_attendance.search');
Route::get('get_attendance/machine/{id}','GetAttendanceController@get_attendance')->name('get_attendance.machine');

Route::get('automatic_fetch_attendance','AutomaticController@fetch_attendance')->name('automatic_fetch_attendance');

/*
|--------------------------------------------------------------------------
| Module SMA MUHAMMADIYAH 1 TAMAN ( V2 )
|--------------------------------------------------------------------------
| 1.
|
*/

Route::get("masuk", "v2\MasukController@masuk")->name("masuk");
Route::post("proses", "v2\MasukController@proses")->name("proses");

Route::get("/homepage", "v2\HomeController@index")
    ->name("home.index");

/*
 | Data Profile Siswa & Wali Murid
 |
 */
//Route::get("profile", "v2\ProfileController@profile")
//->name("profile");

/*
 | Data Absensi Siswa
 | Hanya bisa di akses siswa dan walimurid
 */
Route::get("absensi", "v2\AbsensiController@index")
    ->name("absensi");

/*
 | Data Prestasi
 */
Route::get("prestasi", "v2\PrestasiController@index")
    ->name("prestasi");

/*
 | Data Perizinan
 */
Route::get("perizinan", "v2\PerizinanController@index")
    ->name("perizinan");

Route::get("perizinan/tambah", "v2\PerizinanController@tambah")
    ->name("perizinan.tambah");

Route::post("perizinan/tambah", "v2\PerizinanController@tambah")
    ->name("perizinan.tambah.simpan");

Route::get("perizinan/disetujui/{id}", "v2\PerizinanController@disetujui")
    ->name("perizinan.disetujui");

Route::get("perizinan/konfirmasi/{id}", "v2\PerizinanController@konfirmasi_kembali")
    ->name("perizinan.konfirmasi");

/*
 | Data Pelanggaran
 */
Route::get("pelanggaran", "v2\PelanggaranController@index")
    ->name("pelanggaran");

/*
 | Data Jadwal
 */
Route::get("jadwal", "v2\JadwalController@index")
    ->name("jadwal");
