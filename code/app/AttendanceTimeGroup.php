<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceTimeGroup extends Model
{
    protected $fillable = ['name', 'group', 'date_start', 'date_end'];
}
