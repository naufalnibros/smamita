<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['no_id', 'no_attendance', 'name', 'class'];

    public function attendance($date){
        return $this->hasMany(AttendanceLog::class, 'no_attendance', 'attendance_no_id')
            ->select('time')
            ->where('date','=', date('Y-m-d', strtotime($date)));
    }
}
