<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceMachine extends Model
{
    protected $fillable = ['name', 'ip_address', 'port', 'key', 'flag_active'];
}
