<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TransaksiPerizinan extends Model
{
    protected $table = "transaksi_perizinan";

    public $timestamps = false;

}
