<?php

function credentials(){
    $features = [
        ['url' => 'home', 'caption' => 'Dashboard', 'is_allow' => 1],
        ['url' => '#', 'caption' => 'Manajemen User', 'is_allow' => 1, 'sub_menu' => [
            ['url' => 'user_level', 'caption' => 'User Level', 'is_allow' => 1],
            ['url' => 'user', 'caption' => 'User', 'is_allow' => 1],
            ['url' => 'user_log', 'caption' => 'User Log', 'is_allow' => 1],
        ]],
        ['url' => '#', 'caption' => 'Master Attendance', 'is_allow' => 1, 'sub_menu' => [
            ['url' => 'attendance_machine', 'caption' => 'Attendance Machine', 'is_allow' => 1],
            ['url' => 'attendance_time_group', 'caption' => 'Attendance Time Group', 'is_allow' => 1],
            ['url' => 'holiday', 'caption' => 'Holiday', 'is_allow' => 1],
            ['url' => 'get_attendance', 'caption' => 'Get Attendance', 'is_allow' => 1],
            ['url' => 'automatic_fetch_attendance', 'caption' => 'Get Attendance Automatic', 'is_allow' => 1],
        ]],
        ['url' => 'student', 'caption' => 'Student', 'is_allow' => 1],
        ['url' => 'attendance', 'caption' => 'Attendance', 'is_allow' => 1],
        ['url' => 'attendance_absence', 'caption' => 'Absence', 'is_allow' => 1],
    ];
    return $features;
}

function search_sub_menu($sub_menu,$active_menu){
    $result = false;
    foreach($sub_menu as $item){
        if($item['url'] == $active_menu){
            $result = true;
        }
    }
    return $result;
}

function format_decimal($number){
    if($number != ''){
        return str_replace(".", ",", $number);
    }else{
        return "";
    }
}

function fulldate($date, $divider = " "){
    $dayIndonesia = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
    $monthText = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
    $date = date('Y-m-d',strtotime($date));
    $date_array = explode("-",$date);
    $month = (int)$date_array[1];
    $month = $monthText[$month-1];

    return $dayIndonesia[date('N', strtotime($date))-1].','.$date_array[2].$divider.$month.$divider.$date_array[0];
}

function format_number($number, $currency = 'IDR'){
    if($number != ""){
        if($currency == 'USD') {
            return number_format($number, 2, '.', ',');
        }
        return number_format($number, 0, ',', '.');
    }else{
        return "0";
    }
}

function format_date($date,$divider = "-"){
    if($date != ""){
        $date = date('Y-m-d',strtotime($date));
        $date = explode("-",$date);
        return $date[2].$divider.$date[1].$divider.$date[0];
    }else{
        return "";
    }
}

function time_interval($time_start, $time_end){
    $start_date = new \DateTime($time_start);
    $since_start = $start_date->diff(new \DateTime($time_end));
    $minutes = $since_start->days * 24 * 60;
    $minutes += $since_start->h * 60;
    $minutes += $since_start->i;

    return $minutes;
}
