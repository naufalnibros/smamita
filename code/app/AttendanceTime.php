<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceTime extends Model
{
    protected $fillable = ['attendance_time_group_id', 'name', 'day', 'flag_day_off', 'time_in_start', 'time_in_end',
        'time_out_start', 'time_out_end'];

    public function group_shift(){
        return $this->belongsTo(AttendanceTimeGroup::class);
    }
}
