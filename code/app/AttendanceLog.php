<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceLog extends Model
{
    protected $fillable = ['attendance_machine_id', 'attendance_no_id', 'date', 'time', 'flag', 'media', 'workcode'];

    public function attendance_machine(){
        return $this->belongsTo(AttendanceMachine::class);
    }
}
