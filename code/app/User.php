<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_level(){
        return $this->belongsTo(UserLevel::class);
    }

    public function last_login(){
        return $this->hasOne(UserLog::class)->where('method', '=', 'POST')
            ->where('path', '=', url('/').'/login')
            ->orderBy('id', 'desc');
    }
}
