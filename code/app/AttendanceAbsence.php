<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceAbsence extends Model
{
    protected $fillable = ['student_id', 'date_start', 'date_end', 'code', 'description', 'file_id'];

    public function student(){
        return $this->belongsTo(Student::class);
    }
}
