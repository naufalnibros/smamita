<?php

namespace App\Http\Middleware;

use App\Http\Controllers\IoController;
use Closure;

class UserLogMiddleware
{
    public function handle($request, Closure $next){
        return $next($request);
    }
}
