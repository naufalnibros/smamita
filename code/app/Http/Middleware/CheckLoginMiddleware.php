<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if(Auth::user()->user_level_id != 1){
            $url = str_replace(url('/'),"",$request->url());
            $credentials = json_decode(Auth::user()->user_level->credentials, true);

            $result = false;
            foreach($credentials as $menu){
                if($menu['is_allow'] == 1){
                    if (strpos($url, $menu['url']) !== false) {
                        $result = true;
                    }
                }

                if($menu['url'] == '#'){
                    foreach($menu['sub_menu'] as $sub_menu){
                        if($sub_menu['is_allow'] == 1){
                            if (strpos($url, $sub_menu['url']) !== false) {
                                $result = true;
                            }
                        }
                    }
                }
            }

            if($result == true){
                return $next($request);
            }else{
                return redirect('masuk');
            }
        }else{
            return $next($request);
        }
    }
}
