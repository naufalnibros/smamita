<?php

namespace App\Http\Controllers;

use App\AttendanceLog;
use App\AttendanceMachine;
use Illuminate\Http\Request;

class AutomaticController extends Controller
{
    public function fetch_attendance(Request $request){
        //-----display progress
        ini_set('output_buffering', 'off');
        ini_set('zlib.output_compression', false);
        while(@ob_end_flush());
        ini_set('implicit_flush', true);
        ob_implicit_flush(true);
        echo "Proses Ambil Data ..<br>";
        echo "<table cellpadding='0' cellspacing='0'>";
        echo "<tr><td>Getting Machine Data</td>";
        $machines = AttendanceMachine::where('flag_active', '=', 1)->get();
        echo "<td>".count($machines)." Machine found</td><td>Now Procesing ...</td></tr>";
        foreach($machines as $machine){
            $buffer="";
            $status = 'Not Connected';
            $connect = @fsockopen($machine->ip_address, $machine->port, $errno, $errstr, 1);
            if($connect){
                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">" . $machine->key . "</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($connect, "Content-Type: text/xml".$newLine);
                fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($connect, $soap_request.$newLine);
                while($Response=fgets($connect, 1024)){
                    $buffer=$buffer.$Response;
                    $status = 'Connected';
                }
            }
            echo "<tr><td>Machine ".$machine->name." is ".$status."</td></tr>";
            if($status == "Connected"){
                $last = AttendanceLog::where('attendance_machine_id', '=', $machine->id)
                    ->orderBy('date', 'desc')
                    ->orderBy('time', 'desc')
                    ->first();

                $lastdate = !(empty($last)) ? $last->date : date('1990-01-01');
                $lasttime = !(empty($last)) ? $last->time : date('00:00:00');

                $buffer = $this->parse_data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
                $buffer = explode("\r\n",$buffer);
                array_pop($buffer);
                array_shift($buffer);

                for($i=0; $i<count($buffer); $i++){
                    $row = $this->parse_data($buffer[$i],"<Row>","</Row>");
                    $pin = $this->parse_data($row,"<PIN>","</PIN>");
                    $datetime = $this->parse_data($row,"<DateTime>","</DateTime>");
                    $workcode = $this->parse_data($row,"<WorkCode>","</WorkCode>");
                    $status = $this->parse_data($row,"<Status>","</Status>");
                    $media = $this->parse_data($row,"<Verified>","</Verified>");

                    if(
                        (strtotime(date('Y-m-d', strtotime($datetime))) >= strtotime(date('Y-m-d',strtotime($lastdate))))
                        and
                        (strtotime(date('H', strtotime($datetime))) >= strtotime(date('H',strtotime($lasttime))))
                    ){
                        AttendanceLog::firstOrCreate([
                            'attendance_machine_id' => $machine->id,
                            'attendance_no_id' => $pin,
                            'date' => date('Y-m-d',strtotime($datetime)),
                            'time' => date('H:i:s',strtotime($datetime)),
                            'media' => $media,
                            'workcode' => $workcode,
                        ]);

                        echo "<tr><td>".$pin."</td><td>".$datetime."</td><td>".$media."</td><td>".$workcode."</td><td>".$status."</td></tr>";
                        echo "<script>window.scrollTo(0,document.body.scrollHeight);</script>";
                    }
                }
            }
        }
        echo "</table>";

        return redirect()->route('automatic_fetch_attendance');
    }

    public function parse_data($data,$p1,$p2){
        $data=" ".$data;
        $hasil="";
        $awal=strpos($data,$p1);
        if($awal!=""){
            $akhir=strpos(strstr($data,$p1),$p2);
            if($akhir!=""){
                $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
            }
        }
        return $hasil;
    }
}
