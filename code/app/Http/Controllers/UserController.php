<?php

namespace App\Http\Controllers;

use App\User;
use App\UserLevel;
use App\UserLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'user');
        $search = $request->get('search');
        $data = User::orderBy('name', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }
        foreach($data as $key => $value){
            $data[$key]->last_login = $value->last_login;
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('user.index', compact('search', 'data'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
        }
        return redirect()->route('user', $search);
    }

    public function info(Request $request, $id){
        $field = User::find($id);
        if(!empty($field)){
            $field->user_level = $field->user_level;
        }
        $user_level = UserLevel::orderBy('name', 'asc')->get();
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('user.info', compact('id', 'field', 'user_level'));
    }

    public function save(Request $request, $id){
        if($id == 'new'){
            $this->validate($request, [
                'name' => 'required',
                'user_level_id' => 'required',
                'email' => 'required',
                'password' => 'required|confirmed',
            ], [
                'name.required' => 'Field name is required',
                'user_level_id.required' => 'Field user_level is required',
                'email.required' => 'Field email is required',
                'password.required' => 'Field password is required',
            ]);

            $field = User::create($request->all());
            $message = 'added';
        }else{
            $this->validate($request, [
                'name' => 'required',
                'user_level_id' => 'required',
                'email' => 'required',
            ], [
                'name.required' => 'Field name is required',
                'user_level_id.required' => 'Field user_level is required',
                'email.required' => 'Field email is required',
            ]);

            $field = User::find($id);
            $field->update($request->all());
            $message = 'updated';
        }
        if($request->input('password') != ''){
            $field->password = Hash::make($request->input('password'));
            $field->save();
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('user')
            ->with('message', ['type' => 'success', 'content' => 'User successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = User::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return ['type' => $type, 'data' => $field];
        }
        return redirect()->route('user')
            ->with('message', ['type' => $type, 'content' => 'User deleted '.$message]);
    }
}
