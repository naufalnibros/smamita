<?php

namespace App\Http\Controllers;

use App\AttendanceAbsence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AttendanceAbsenceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'attendance_absence');
        $search = $request->get('search');
        $data = AttendanceAbsence::orderBy('date_start', 'desc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('attendance_absence.index', compact('search', 'data'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
        }
        return redirect()->route('attendance_absence', $search);
    }

    public function info(Request $request, $id){
        $field = AttendanceAbsence::find($id);
        $code_array = [
            ['id' => 'S', 'caption' => 'Sakit'],
            ['id' => 'I', 'caption' => 'Izin'],
        ];

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('attendance_absence.info', compact('id', 'field', 'code_array'));
    }

    public function save(Request $request, $id){
        $this->validate($request, [
            'student_id' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'code' => 'required',
            'description' => 'required',
        ]);

        $request->merge(['date_start' => date('Y-m-d', strtotime($request->input('date_start')))]);
        $request->merge(['date_end' => date('Y-m-d', strtotime($request->input('date_end')))]);

        if($id == 'new'){
            $field = AttendanceAbsence::create($request->all());
            $message = 'added';
        }else{
            $field = AttendanceAbsence::find($id);
            $field->update($request->all());
            $message = 'updated';
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('attendance_absence')
            ->with('message', ['type' => 'success', 'content' => 'Attendance Absence successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = AttendanceAbsence::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return ['type' => $type, 'data' => $field];
        }
        return redirect()->route('attendance_absence')
            ->with('message', ['type' => $type, 'content' => 'Attendance Absence deleted '.$message]);
    }
}
