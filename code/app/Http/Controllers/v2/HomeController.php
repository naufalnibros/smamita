<?php


namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\MasterPrestasi;
use App\Models\MasterSiswa;
use App\Models\TransaksiPelanggaran;
use App\Models\TransaksiPerizinan;

class HomeController extends Controller {

    public function index(){
        if (!session()->has("user")) {
            session()->flush();
            return redirect("masuk");
        }

        if (session()->get("user")->type === "siswa" || session()->get("user")->type === "walimurid") {
            $type_master_user =  session()->get("user")->type === "siswa" ? "master_user_id_siswa" : "master_user_id_walimurid";

            $siswa = MasterSiswa::where($type_master_user, session()->get("user")->id)->first();

            $jumlah = array(
                "perizinan" => TransaksiPerizinan::where("master_siswa_id", $siswa->id)->count(),
                "pelanggaran" => TransaksiPelanggaran::where("m_siswa_id", $siswa->id)->count(),
                "prestasi" => MasterPrestasi::where("master_siswa_id", $siswa->id)->count()
            );
        } else {
            $jumlah = array(
                "perizinan" => TransaksiPerizinan::where("master_user_id", session()->get("user")->id)->count(),
                "pelanggaran" => TransaksiPelanggaran::count(),
                "prestasi" => MasterPrestasi::count()
            );
        }

        return view("home")
            ->with("jumlah", $jumlah);
    }

}
