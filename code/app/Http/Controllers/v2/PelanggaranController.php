<?php


namespace App\Http\Controllers\v2;


use App\Http\Controllers\Controller;
use App\Models\MasterPrestasi;
use App\Models\MasterSiswa;
use Illuminate\Http\Request;

class PelanggaranController extends Controller
{
    public function index(Request $request){
        if (!session()->has("user")) {
            session()->flush();
            return redirect("masuk");
        }

        $prestasi = MasterPrestasi::select("master_prestasi.*", "master_siswa.nama as siswa_nama", "master_siswa.nipd as siswa_nipd", "master_user.nama as user_nama")->orderBy("id", "DESC");

        if (session()->get("user")->type === "siswa" || session()->get("user")->type === "walimurid") {
            $type_master_user =  session()->get("user")->type === "siswa" ? "master_user_id_siswa" : "master_user_id_walimurid";
            $siswa = MasterSiswa::where($type_master_user, session()->get("user")->id)->first();

            $prestasi->where("master_siswa_id", $siswa->id);
        }

        $prestasi->leftJoin("master_siswa", "master_siswa.id", "=", "master_prestasi.master_siswa_id");
        $prestasi->leftJoin("master_user", "master_user.id", "=", "master_prestasi.modified_by");

        return view("pelanggaran.index")
            ->with("models", $prestasi->paginate(10));
    }
}
