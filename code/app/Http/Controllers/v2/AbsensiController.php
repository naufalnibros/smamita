<?php


namespace App\Http\Controllers\v2;


use App\Http\Controllers\Controller;
use App\Models\MasterSiswa;
use App\Models\TransaksiFinger;
use Illuminate\Support\Facades\DB;

class AbsensiController extends Controller {

    public function index() {
        if (!session()->has("user")) {
            session()->flush();
            return redirect("masuk");
        }

        $models = array();

        if (session()->get("user")->type === "siswa" || session()->get("user")->type === "walimurid") {
            $type_master_user =  session()->get("user")->type === "siswa" ? "master_user_id_siswa" : "master_user_id_walimurid";
            $siswa = MasterSiswa::where($type_master_user, session()->get("user")->id)->first();

            $datang = TransaksiFinger::where("unique_id", $siswa->nipd)
                ->where("status", 0)->orderBy("datetime", "desc")->get();

            foreach ($datang as $value) {
                $pulang = TransaksiFinger::where("unique_id", "=", $siswa->nipd)
                    ->where("status", "=", 1)
                    ->where(DB::raw("DATE(datetime)"), date("Y-m-d", strtotime($value->datetime)))
                    ->first();

                $models[] = array(
                    "datang" => $value,
                    "pulang" => $pulang,
                    "tanggal" => $this->tgl_indo(strtotime($value->datetime)),
                    "jam_masuk" => date("H:i:s", strtotime($value->datetime)),
                    "jam_pulang" => empty($pulang) ? "-" : date("H:i:s", strtotime($pulang->datetime)),
                    "keterangan" => $this->keterangan($value, $pulang)
                );
            }
            return view("absensi.index")->with("models", $models);
        }
        return redirect("homepage");
    }

    /**
     * format hari
     *
     * @param String from date ("D");
     * @return String
     */
    function format_hari($hari) {
        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;
            case 'Mon':
                $hari_ini = "Senin";
                break;
            case 'Tue':
                $hari_ini = "Selasa";
                break;
            case 'Wed':
                $hari_ini = "Rabu";
                break;
            case 'Thu':
                $hari_ini = "Kamis";
                break;
            case 'Fri':
                $hari_ini = "Jumat";
                break;
            case 'Sat':
                $hari_ini = "Sabtu";
                break;
            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        return $hari_ini;
    }

    function tgl_indo($datetime){
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split = explode('-', date("Y-m-d",$datetime));
        return $this->format_hari(date("D", $datetime)) . ', ' . $split[2]  . ' ' . $bulan[(int) $split[1]] . ' ' . $split[0];
    }

    /**
     * Keterangan Absen
     *
     * @param Object $datang
     * @param Object $pulang
     * @return String
     */
    function keterangan($datang, $pulang){
        $jam_datang = array(
            "awal" => strtotime("05:00:00"),
            "akhir" => strtotime("07:00:00"),
        );
        $jam_pulang = array(
            "awal" => strtotime("15:30:00"),
            "akhir" => strtotime("17:00:00"),
        );
        if (empty($pulang)) {
            return "TANPA KETERANGAN";
        }
        if (($jam_datang["awal"] <= $this->to_time($datang->datetime)) && ($this->to_time($datang->datetime) <=  $jam_datang["akhir"])) {
            if (!(($jam_pulang["awal"] <= $this->to_time($pulang->datetime)) && ($this->to_time($pulang->datetime) <=  $jam_pulang["akhir"]))) {
                if (!($jam_pulang["awal"] <= $this->to_time($pulang->datetime))) { return "PULANG AWAL"; }
                return "TANPA KETERANGAN";
            }
        } else {
            return "TERLAMBAT";
        }
        return "MASUK";
    }

    function to_time($datetime){
        return strtotime(date("H:i:s", strtotime($datetime)));
    }

}
