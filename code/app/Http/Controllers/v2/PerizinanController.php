<?php


namespace App\Http\Controllers\v2;


use App\Http\Controllers\Controller;
use App\Models\MasterGuru;
use App\Models\MasterSiswa;
use App\Models\TransaksiPerizinan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class PerizinanController extends Controller
{
    public function index(Request $request){
        if (!session()->has("user")) {
            session()->flush();
            return redirect("masuk");
        }

        $perizinan = TransaksiPerizinan::select(DB::raw("transaksi_perizinan.*, master_siswa.nama as siswa_nama, master_siswa.nipd as siswa_nipd,
        (SELECT master_guru.nama FROM master_guru WHERE master_guru.master_user_id = master_user_id ORDER BY master_guru.id ASC LIMIT 1) as guru_nama"))
            ->orderBy("jam_keluar", "desc");

        if (session()->get("user")->type === "siswa" || session()->get("user")->type === "walimurid") {
            $type_master_user =  session()->get("user")->type === "siswa" ? "master_user_id_siswa" : "master_user_id_walimurid";
            $siswa = MasterSiswa::where($type_master_user, session()->get("user")->id)->first();

            if (!empty($request->keyword)) {
                $perizinan->orWhere('transaksi_perizinan.keterangan', 'like', "%{$request->keyword}%");
            }
            $perizinan->where("master_siswa_id", $siswa->id);
        } else {
            $perizinan->where("master_user_id", session()->get("user")->id);
        }

        $perizinan->leftJoin("master_siswa", "master_siswa.id", "=", "transaksi_perizinan.master_siswa_id");

        return view("perizinan.index")
            ->with("models", $perizinan->paginate(10));
    }

    public function tambah(Request $request){
        try {
            if ($request->method() !== "GET") {
                $type_master_user =  session()->get("user")->type === "siswa" ? "master_user_id_siswa" : "master_user_id_walimurid";
                $siswa = MasterSiswa::where($type_master_user, session()->get("user")->id)->first();

                DB::table('transaksi_perizinan')->insert([
                    "master_user_id" => $request->master_user_id,
                    "master_siswa_id" => $siswa->id,
                    "keterangan" => empty($request->keterangan) ? "Tidak Disi" : $request->keterangan,
                    "jam_keluar" => date("Y-m-d H:i:s", strtotime(date("Y-m-d") . " " . $request->jam_keluar)),
                    "jam_kembali" => date("Y-m-d H:i:s", strtotime(date("Y-m-d") . " " .$request->jam_kembali))
                ]);

                return redirect()->route("perizinan");
            }
        } catch (\Exception $e){
            return $e;
        }

        return view("perizinan.info")
            ->with("guru", MasterGuru::get())
            ->with("model", NULL);
    }

    public function detail($id){

    }

    public function save(Request $request){

    }

    public function disetujui($id){
        if (session()->get("user")->type === "guru"){

            TransaksiPerizinan::where("id", $id)
                ->update([
                    "is_approved" => 1,
                    "approved_at" => date("Y-m-d H:i:s")
                ]);
        }

        return redirect()->route("perizinan");
    }

    public function konfirmasi_kembali($id){
        if (session()->get("user")->type === "guru"){

            TransaksiPerizinan::where("id", $id)
                ->update([
                    "is_kembali" => 1,
                    "kembali_at" => date("Y-m-d H:i:s")
                ]);
        }

        return redirect()->route("perizinan");
    }

}
