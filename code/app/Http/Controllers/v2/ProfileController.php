<?php


namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    public function profile(){
        if (session()->get("user")->type === "siswa" || session()->get("user")->type === "walimurid") {
            return view("profile.siswa");
        }

        return view("profile.guru");
    }

}
