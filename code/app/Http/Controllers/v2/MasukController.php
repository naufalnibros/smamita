<?php


namespace App\Http\Controllers\v2;


use App\Http\Controllers\Controller;
use App\Models\MasterUser;
use Illuminate\Http\Request;

class MasukController extends Controller
{

    public function masuk() {
        return view('masuk');
    }

    public function proses(Request $request) {
        $masterUser = MasterUser::where("username", $request->username)->where("password", sha1($request->password))
                            ->where("type", "!=", "admin")->first();
        if (!empty($masterUser)) {
            session()->put("user", $masterUser);

            return redirect()->route("home.index");
        } else {
            return view("masuk")
                ->with("message", ["username" => "Username tidak ditemukan", "password" => "Password Salah"]);
        }
    }

}
