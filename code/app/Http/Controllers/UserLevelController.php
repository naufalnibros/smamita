<?php

namespace App\Http\Controllers;

use App\UserLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserLevelController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'user_level');
        $search = $request->get('search');
        $data = UserLevel::orderBy('name', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('user_level.index', compact('search', 'data'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= $request->input('filter').',like,'.$request->input('keyword').';';
        }
        return redirect()->route('user_level', $search);
    }

    public function info(Request $request, $id){
        $field = UserLevel::find($id);
        if($id != 'new'){
            $selected_credentials = '';
            if($field->credentials != ''){
                $credentials = json_decode($field->credentials, true);
                foreach($credentials as $key => $value){
                    $checked = $value['is_allow'] == 1 ? true : false;
                    $credentials[$key]['state']['checked'] = $checked;
                    $selected_credentials .= $value['is_allow'] == 1 ? $value['caption'].',' : '';
                    if(!empty($value['sub_menu'])){
                        foreach($value['sub_menu'] as $key2 => $value2){
                            $checked2 = $value2['is_allow'] == 1 ? true : false;
                            $credentials[$key]['sub_menu'][$key2]['state']['checked'] = $checked2;
                            $selected_credentials .= $value2['is_allow'] == 1 ? $value2['caption'].',' : '';
                        }
                    }
                }
            }else{
                $credentials = credentials();
            }
            $selected_credentials = substr($selected_credentials, 0 ,strlen($selected_credentials)-1);
            $field->selected_credentials = $selected_credentials;
        }else{
            $credentials = credentials();
        }

        foreach($credentials as $key => $value){
            $credentials[$key]['text'] = $value['caption'];
            if(!empty($value['sub_menu'])){
                foreach($value['sub_menu'] as $key2 => $value2){
                    $credentials[$key]['sub_menu'][$key2]['text'] = $value2['caption'];
                }
                $credentials[$key]['children'] = $credentials[$key]['sub_menu'];
            }
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('user_level.info', compact('id', 'field', 'credentials'));
    }

    public function save(Request $request, $id){
        $this->validate($request, [
            'name' => 'required'
        ], [
            'name.required' => 'Field name is required'
        ]);

        $credentials = credentials();
        $selected_credentials = explode(",", $request->input('selected_credentials'));
        foreach($credentials as $key => $value){
            $credentials[$key]['is_allow'] = ($this->check_credentials($selected_credentials, $value['caption'])) ? 1 : 0;
            if(!empty($value['sub_menu'])){
                foreach($value['sub_menu'] as $key2 => $value2){
                    $credentials[$key]['sub_menu'][$key2]['is_allow'] = ($this->check_credentials($selected_credentials, $value2['caption'])) ? 1 : 0;
                }
            }
        }

        $request->merge(['credentials' => json_encode($credentials)]);

        if($id == 'new'){
            $field = UserLevel::create($request->all());
            $message = 'added';
        }else{
            $field = UserLevel::find($id);
            $field->update($request->all());
            $message = 'updated';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('user_level')
            ->with('message', ['type' => 'success', 'content' => 'User Level successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = UserLevel::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('user_level')
            ->with('message', ['type' => $type, 'content' => 'User Level deleted '.$message]);
    }

    public function credentials(Request $request, $id){
        $user_level = UserLevel::find($id);
        $user_credentials = ($user_level->credentials != '') ? json_decode($user_level->credentials, true) : credentials();
        $data = credentials();
        foreach($data as $key => $value){
            $data[$key]['text'] = $value['caption'];
            $data[$key]['icon'] = $user_credentials[$key]['is_allow'] == 1 ? 'fa fa-check' : 'fa fa-times';
            if(!empty($value['sub_menu'])){
                foreach($value['sub_menu'] as $key2 => $value2){
                    $data[$key]['sub_menu'][$key2]['text'] = $value2['caption'];
                    $data[$key]['sub_menu'][$key2]['icon'] = $user_credentials[$key]['sub_menu'][$key2]['is_allow'] == 1 ? 'fa fa-check' : 'fa fa-times';
                }
                $data[$key]['children'] = $data[$key]['sub_menu'];
            }
        }
        return $data;
    }

    public function check_credentials($credentials, $menu){
        $result = false;
        foreach($credentials as $value){
            if($value == $menu){
                $result = true;
            }
        }
        return $result;
    }
}
