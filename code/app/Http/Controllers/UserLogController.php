<?php

namespace App\Http\Controllers;

use App\User;
use App\UserLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserLogController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'user');
        $search = $request->get('search');
        $data = User::orderBy('name', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }
        $log_data = [];
        $user_id = $request->has('user_id') ? $request->get('user_id') : null;
        $date = $request->has('date') ? $request->get('date') : date('d-m-Y');
        if($request->has('user_id')){
            $log_data = UserLog::where('user_id', '=', $request->get('user_id'))
                ->where('created_at', 'like', date('Y-m-d', strtotime($date)).'%')
                ->orderBy('created_at', 'desc')
                ->get();
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('user_log.index', compact('search', 'data', 'date', 'user_id', 'log_data'));
    }

    public function search(Request $request){
        $parameters = [];
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
            array_push($parameters, $search);
        }
        if($request->has('date')){
            array_push($parameters, 'user_id='.$request->input('user_id'));
            array_push($parameters, 'date='.$request->input('date'));
        }
        return redirect()->route('user_log', $parameters);
    }
}
