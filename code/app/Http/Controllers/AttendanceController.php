<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\AttendanceAbsence;
use App\AttendanceLog;
use App\AttendanceSummary;
use App\AttendanceTime;
use App\AttendanceTimeGroup;
use App\CustomShift;
use App\CustomWorkingHour;
use App\Division;
use App\Employee;
use App\GroupShift;
use App\Holiday;
use App\Student;
use App\WorkingHour;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AttendanceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active','attendance');

        $student = [];
        $days_in_month = 0;
        if(($request->get('month') != null)){
            $days_in_month = date('t', strtotime('01'. $request->input('month')));
            $student = Student::orderBy('no_id', 'asc');
            if($request->has('search')){
                $student = $student->where('name', 'like', '%'.$request->get('search').'%');
            }
            $student = $student->paginate(10);

            //--get list employee_id in division
            $list_employee_id = Student::select('no_attendance');
            if($request->has('search')){
                $list_employee_id = $list_employee_id->where('name', 'like', '%'.$request->get('search').'%');
            }
            $list_employee_id = $list_employee_id->get();

            $all_attendance_log = AttendanceLog::where('date','like',date('Y-m',strtotime('01-'.$request->get('month'))).'-%')
                ->whereIn('attendance_no_id', $list_employee_id)
                ->orderBy('attendance_no_id','asc')
                ->orderBy('date','asc')
                ->orderBy('time','asc')
                ->get();

            //--process data employee
            foreach($student as $key => $value){
                $group_shift = AttendanceTimeGroup::where('group', '=', $value->group_attendance)
                    ->where('date_start', '<=', date('Y-m-d'))
                    ->where('date_end', '>=', date('Y-m-d'))
                    ->first();

                $attendance_log = $this->search_attendance($all_attendance_log, $request->get('month'), $value->no_attendance);
                $attendance = [];

                if(!empty($group_shift)){
                    for($i = 1; $i <= $days_in_month; $i++){
                        $working_hour = AttendanceTime::where('attendance_time_group_id', '=', $group_shift->id)
                            ->where('day', '=', date('N', strtotime($i.'-'.$request->get('month'))))
                            ->first();
                        $holiday = Holiday::where('date_start', '<=', date('Y-m-d', strtotime($i.'-'.$request->get('month'))))
                            ->where('date_end', '>=', date('Y-m-d', strtotime($i.'-'.$request->get('month'))))
                            ->first();
                        $absence = AttendanceAbsence::where('date_start', '<=', date('Y-m-d', strtotime($i.'-'.$request->get('month'))))
                            ->where('date_end', '>=', date('Y-m-d', strtotime($i.'-'.$request->get('month'))))
                            ->where('student_id', '=', $value->id)
                            ->first();

                        if(empty($working_hour)){
                            $message['type'] = 'danger';
                            $message['content'] = "Student : ".$value->no_id." ".$value->name." Shift Empty";
                            return redirect()->route('attendance')
                                ->with('message',$message);
                        }
                        $attendance_log_item = $attendance_log[$i-1];

                        $working_hour_log['time_in_start'] = date('H:i:s', strtotime($working_hour->time_in_start));
                        $working_hour_log['time_in_end'] = date('H:i:s', strtotime($working_hour->time_in_end));
                        $working_hour_log['time_out_start'] = date('H:i:s', strtotime($working_hour->time_out_start));
                        $working_hour_log['time_out_end'] = date('H:i:s', strtotime($working_hour->time_out_end));

                        $working_hour_log['time_in_late_1'] = date('H:i:s', strtotime('+5 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_in_late_2'] = date('H:i:s', strtotime('+30 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_in_late_3'] = date('H:i:s', strtotime('+60 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_in_late_4'] = date('H:i:s', strtotime('+120 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_in_late_last'] = date("H:i:s", strtotime("12:00:00"));

                        $working_hour_log['time_out_early_1'] = date('H:i:s', strtotime('-5 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_out_early_2'] = date('H:i:s', strtotime('-30 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_out_early_3'] = date('H:i:s', strtotime('-60 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_out_early_4'] = date('H:i:s', strtotime('-120 minutes', strtotime($working_hour->time_in_end)));
                        $working_hour_log['time_out_early_last'] = date("H:i:s", strtotime("12:00:01"));

                        $code = 'TK';
                        $description = 'Tanpa Keterangan';
                        $flag_attendance = 0;
                        $time_in = null;
                        $time_out = null;
                        $work_interval = 0;

                        if(!empty($holiday)){
                            $code = 'Lbr';
                            $description = $holiday->name;
                        }elseif(!empty($absence)){
                            $code = $absence->code;
                            $description = $absence->description;
                        }elseif($working_hour->flag_day_off == 1){
                            $code = 'Lbr';
                            $description = $working_hour->name;
                        }else{
                            if(count($attendance_log_item) > 0){
                                if(($attendance_log_item[0] >= $working_hour->time_in_start) and ($attendance_log_item[0] <= $working_hour->time_in_end)){
                                    $time_in = $attendance_log_item[0];
                                    $code = 'H';
                                    $description = 'Hadir';
                                }else{
                                    $time_in = $attendance_log_item[0];
                                    $code = 'TL';
                                    $description = 'Terlambat';
                                }

                                rsort($attendance_log_item,1);
                                if(($attendance_log_item[0] >= $working_hour->time_out_start) and ($attendance_log_item[0] <= $working_hour->time_out_end)){
                                    $time_out = $attendance_log_item[0];
                                }else{
                                    $time_out = $attendance_log_item[0];
                                    if($code == 'H'){
                                        $code = 'PA';
                                        $description = 'Pulang awal';
                                    }else{
                                        $code = $code.'-PA';
                                        $description = $description.' dan Pulang awal';
                                    }
                                }
                            }

                            if(($time_in != null) and $time_out != null){
                                $flag_attendance = 1;
                                $work_interval = time_interval($i.'-'.$request->get('month').' '.$time_in, $i.'-'.$request->get('month').' '.$time_out);
                            }
                        }

                        $item_attendance = Attendance::firstOrNew([
                            'student_id' => $value->id,
                            'date' => date('Y-m-d', strtotime($i.'-'.$request->get('month'))),
                        ]);
                        $item_attendance->time_log = json_encode($attendance_log[$i-1]);
                        $item_attendance->time_in = $time_in;
                        $item_attendance->time_out = $time_out;
                        $item_attendance->work_interval = $work_interval;
                        $item_attendance->code = $code;
                        $item_attendance->flag_attendance = $flag_attendance;
                        $item_attendance->description = $description;
                        $item_attendance->attendance_time = json_encode($working_hour_log);
                        $item_attendance->save();

                        array_push($attendance, $item_attendance);
                    }
                }else{
                    $message['type'] = 'danger';
                    $message['content'] = "Group Shift Error";
                    return redirect()->route('attendance')
                        ->with('message',$message);
                }

                $student[$key]->attendance = $attendance;
                $student[$key]->attendance_log = $attendance_log;
            }
        }

        return view('attendance.index')
            ->with('month', $request->get('month'))
            ->with('search', $request->get('search'))
            ->with('employee', $student)
            ->with('days_in_month', $days_in_month);
    }

    public function time_interval($time_start, $time_end){
        $start_date = new \DateTime($time_start);
        $since_start = $start_date->diff(new \DateTime($time_end));
        $minutes = $since_start->days * 24 * 60;
        $minutes += $since_start->h * 60;
        $minutes += $since_start->i;

        return $minutes;
    }

    public function search_attendance($attendance_log, $month, $attendance_no_id){
        $result = [];
        $days_in_month = date('t', strtotime('01'. $month));
        for($i = 0; $i < $days_in_month; $i++){
            $result[$i] = [];
        }
        foreach($attendance_log as $key => $value){
            if($value->attendance_no_id == $attendance_no_id){
                array_push($result[date('j', strtotime($value->date))-1], $value->time);
            }
        }
        return $result;
    }

    public function search(Request $request){
        $parameters = [];
        if ($request->input('month') != '') {
            array_push($parameters, 'month=' . $request->input('month'));
        }
        if ($request->input('search') != '') {
            array_push($parameters, 'search=' . $request->input('search'));
        }
        return redirect()->route('attendance', $parameters);
    }
}
