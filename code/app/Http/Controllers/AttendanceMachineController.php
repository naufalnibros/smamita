<?php

namespace App\Http\Controllers;

use App\AttendanceMachine;
use App\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AttendanceMachineController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'holiday');
        $search = $request->get('search');
        $data = AttendanceMachine::orderBy('name', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }
        $check_status = $request->get('check_status');
        if($request->has('check_status')){
            foreach ($data as $key => $value){
                $data[$key]->status = $this->machine_status($value->id);
            }
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('attendance_machine.index', compact('search', 'data', 'check_status'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
        }
        return redirect()->route('attendance_machine', $search);
    }

    public function info(Request $request, $id){
        $field = AttendanceMachine::find($id);

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('attendance_machine.info', compact('id', 'field'));
    }

    public function save(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'ip_address' => 'required',
            'port' => 'required',
            'key' => 'required',
        ], [
            'name.required' => 'Field name is required',
            'ip_address.required' => 'Field ip_address is required',
            'port.required' => 'Field port is required',
            'key.required' => 'Field key is required',
        ]);

        if(!$request->has('flag_active')){
            $request->merge(['flag_active' => '0']);
        }

        if($id == 'new'){
            $field = AttendanceMachine::create($request->all());
            $message = 'added';
        }else{
            $field = AttendanceMachine::find($id);
            $field->update($request->all());
            $message = 'updated';
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('attendance_machine')
            ->with('message', ['type' => 'success', 'content' => 'Attendance Machine successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = AttendanceMachine::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return ['type' => $type, 'data' => $field];
        }
        return redirect()->route('attendance_machine')
            ->with('message', ['type' => $type, 'content' => 'Attendance Machine deleted '.$message]);
    }

    public function machine_status($id){
        $machine = AttendanceMachine::find($id);
        $result = 'Not Connected';
        $connect = @fsockopen($machine->ip_address, $machine->port, $errno, $errstr, 1);
        if($connect){
            $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$machine->key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
            $newLine="\r\n";
            fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
            fputs($connect, "Content-Type: text/xml".$newLine);
            fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
            fputs($connect, $soap_request.$newLine);
            while($Response=fgets($connect, 1024)){
                $result = 'Connected';
            }
        }
        return $result;
    }

    public function parse_data($data,$p1,$p2){
        $data=" ".$data;
        $hasil="";
        $awal=strpos($data,$p1);
        if($awal!=""){
            $akhir=strpos(strstr($data,$p1),$p2);
            if($akhir!=""){
                $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
            }
        }
        return $hasil;
    }
}
