<?php

namespace App\Http\Controllers;

use App\Notification;
use App\UserLog;
use Illuminate\Support\Facades\Auth;

class IoController extends Controller
{

    public function search_tool($query,$search){
        if($search != null){
            $search_array = explode(";",$search);
            array_pop($search_array);
            foreach($search_array as $value){
                $search_part = explode(',',$value);
                if(($value != 'all') and ($search_part[2] != 'all')){
                    if((strpos($search_part[0], 'tanggal') !== false) or (strpos($search_part[0], 'date') !== false)) {
                        $keyword = $search_part[2];
                        if(strlen($keyword) == 7){
                            $query = $query->where($search_part[0],$search_part[1],date('Y-m',strtotime('01-'.$keyword)).'-%');
                        }else{
                            $query = $query->where($search_part[0],$search_part[1],date('Y-m-d',strtotime($keyword)));
                        }
                    }else{
                        $keyword = $search_part[2];
                        if($search_part[1] == 'like'){
                            $keyword = '%'.$search_part[2].'%';
                        }
                        $keyword = str_replace("+","/",$keyword);
                        $query = $query->where($search_part[0],$search_part[1],$keyword);
                    }
                }
            }
        }
        return $query;
    }

    public function save_user_log($request){
        $user_id = (Auth::check()) ? Auth::user()->id : null;
        if(!empty($request->all())){
            UserLog::create([
                'user_id' => $user_id,
                'path' => $request->fullUrl(),
                'ip' => $request->getClientIp(),
                'method' => $request->method(),
                'request_data' => json_encode($request->all()),
            ]);
        }
    }

    public function save_notif($nip, $content){
        Notification::create([
            'nip' => $nip,
            'content' => $content
        ]);
    }
}
