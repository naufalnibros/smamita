<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StudentController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'student');
        $search = $request->get('search');
        $data = Student::orderBy('name', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('student.index', compact('search', 'data'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
        }
        return redirect()->route('student', $search);
    }

    public function info(Request $request, $id){
        $field = Student::find($id);

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('student.info', compact('id', 'field'));
    }

    public function save(Request $request, $id){
        $this->validate($request, [
            'no_id' => 'required',
            'name' => 'required',
            'no_attendance' => 'required',
            'class' => 'required',
        ], [
            'no_id.required' => 'Field no_id is required',
            'name.required' => 'Field name is required',
            'no_attendance.required' => 'Field no_attendance is required',
            'class.required' => 'Field class is required',
        ]);

        if($id == 'new'){
            $field = Student::create($request->all());
            $message = 'added';
        }else{
            $field = Student::find($id);
            $field->update($request->all());
            $message = 'updated';
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('student')
            ->with('message', ['type' => 'success', 'content' => 'Student successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = Student::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return ['type' => $type, 'data' => $field];
        }
        return redirect()->route('student')
            ->with('message', ['type' => $type, 'content' => 'Student deleted '.$message]);
    }
}
