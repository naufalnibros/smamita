<?php

namespace App\Http\Controllers;

use App\AttendanceTimeGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AttendanceTimeGroupController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'attendance_time_group');
        $search = $request->get('search');
        $data = AttendanceTimeGroup::orderBy('name', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('attendance_time_group.index', compact('search', 'data'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
        }
        return redirect()->route('attendance_time_group', $search);
    }

    public function info(Request $request, $id){
        $field = AttendanceTimeGroup::find($id);

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('attendance_time_group.info', compact('id', 'field'));
    }

    public function save(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'group' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
        ], [
            'name.required' => 'Field name is required',
            'group.required' => 'Field name is required',
            'date_start.required' => 'Field date_start is required',
            'date_end.required' => 'Field date_end is required',
        ]);

        $request->merge(['date_start' => date('Y-m-d', strtotime($request->input('date_start')))]);
        $request->merge(['date_end' => date('Y-m-d', strtotime($request->input('date_end')))]);

        if($id == 'new'){
            $field = AttendanceTimeGroup::create($request->all());
            $message = 'added';
        }else{
            $field = AttendanceTimeGroup::find($id);
            $field->update($request->all());
            $message = 'updated';
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('attendance_time_group')
            ->with('message', ['type' => 'success', 'content' => 'Attendance Time Group successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = AttendanceTimeGroup::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return ['type' => $type, 'data' => $field];
        }
        return redirect()->route('attendance_time_group')
            ->with('message', ['type' => $type, 'content' => 'Attendance Time Group deleted '.$message]);
    }
}
