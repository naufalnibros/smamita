<?php

namespace App\Http\Controllers;

use App\AttendanceTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AttendanceTimeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('check_login');
    }

    public function index(Request $request){
        Session::put('menu_active', 'attendance_time');
        $search = $request->get('search');
        $data = AttendanceTime::orderBy('day', 'asc');
        $ioController = new IoController();
        $data = $ioController->search_tool($data, $search);
        if($request->has('show') and $request->get('show') == 'all'){
            $data = $data->get();
        }else{
            $data = $data->paginate(10);
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $data;
        }
        return view('attendance_time.index', compact('search', 'data'));
    }

    public function search(Request $request){
        $search = '';
        if($request->input('keyword') != ''){
            $search = $search == '' ? 'search=' : $search;
            $search .= 'name,like,'.$request->input('keyword').';';
        }
        return redirect()->route('attendance_time', $search);
    }

    public function info(Request $request, $id){
        $field = AttendanceTime::find($id);

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return view('attendance_time.info', compact('id', 'field'));
    }

    public function save(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
            'day' => 'required',
            'flag_day_off' => 'required',
        ], [
            'name.required' => 'Field name is required',
            'day.required' => 'Field day is required',
            'flag_day_off.required' => 'Field flag_day_off is required',
        ]);

        if($request->input('flag_day_off') == '1'){
            $request->merge(['time_in_start' => null]);
            $request->merge(['time_in_end' => null]);
            $request->merge(['time_out_start' => null]);
            $request->merge(['time_out_end' => null]);
        }

        if($id == 'new'){
            $field = AttendanceTime::create($request->all());
            $message = 'added';
        }else{
            $field = AttendanceTime::find($id);
            $field->update($request->all());
            $message = 'updated';
        }

        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return $field;
        }
        return redirect()->route('attendance_time')
            ->with('message', ['type' => 'success', 'content' => 'Attendance Time Group successfully '.$message]);
    }

    public function delete(Request $request, $id){
        $field = AttendanceTime::find($id);
        try {
            $field->delete();
            $type = 'success';
            $message = 'successfully';
        } catch (\Exception $e) {
            $type = 'error';
            $message = 'failed';
        }
        if($request->has('mode') and $request->get('mode') == 'ajax'){
            return ['type' => $type, 'data' => $field];
        }
        return redirect()->route('attendance_time')
            ->with('message', ['type' => $type, 'content' => 'Attendance Time Group deleted '.$message]);
    }
}
