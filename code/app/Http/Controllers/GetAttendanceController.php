<?php

namespace App\Http\Controllers;

use App\AttendanceLog;
use App\AttendanceMachine;
use App\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class GetAttendanceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request){
        Session::put('menu_active', 'attendance');
        $query = AttendanceMachine::select('*')->orderBy('id','desc');
        $ioController = new IoController();
        $query = $ioController->search_tool($query,$request->get('search'));
        $data = $query->paginate(10);
        $attendanceMachineController = new AttendanceMachineController();
        foreach ($data as $key => $value){
            $data[$key]->status = $attendanceMachineController->machine_status($value->id);
        }

        return view('get_attendance.index')
            ->with('data', $data)
            ->with('month', $request->get('month'));
    }

    public function search(Request $request){
        $search = [];

        if($request->input('fid_satker') != '') {
            array_push($search, 'fid_satker=' . $request->input('fid_satker'));
        }
        if($request->input('month') != '') {
            array_push($search, 'month=' . $request->input('month'));
        }
        return redirect()->route('get_attendance', $search);
    }

    public function get_attendance(Request $request, $id){
        // Turn off output buffering
        ini_set('output_buffering', 'off');
        // Turn off PHP output compression
        ini_set('zlib.output_compression', false);

        //Flush (send) the output buffer and turn off output buffering
        while(@ob_end_flush());

        // Implicitly flush the buffer(s)
        ini_set('implicit_flush', true);
        ob_implicit_flush(true);

        $attendance_machine = AttendanceMachine::find($id);
        $buffer="";
        $connect = @fsockopen($attendance_machine->ip_address, $attendance_machine->port, $errno, $errstr, 1);
        if($connect){
            $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">" . $attendance_machine->key . "</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
            $newLine="\r\n";
            fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
            fputs($connect, "Content-Type: text/xml".$newLine);
            fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
            fputs($connect, $soap_request.$newLine);
            while($Response=fgets($connect, 1024)){
                $buffer=$buffer.$Response;
                $result['status'] = 'Connected';
            }
        }
        $buffer = $this->parse_data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
        $buffer = explode("\r\n",$buffer);
        array_pop($buffer);
        array_shift($buffer);

        $last = AttendanceLog::where('attendance_machine_id', '=', $id)
            ->orderBy('date', 'desc')
            ->orderBy('time', 'desc')
            ->first();

        $lastdate = !(empty($last)) ? $last->date : date('1990-01-01');
        $lasttime = !(empty($last)) ? $last->time : date('00:00:00');

        echo "Proses Ambil Data ..<br>";
        echo "<table>";
        for($i=0; $i<count($buffer); $i++){
            $row = $this->parse_data($buffer[$i],"<Row>","</Row>");
            $pin = $this->parse_data($row,"<PIN>","</PIN>");
            $datetime = $this->parse_data($row,"<DateTime>","</DateTime>");
            $workcode = $this->parse_data($row,"<WorkCode>","</WorkCode>");
            $status = $this->parse_data($row,"<Status>","</Status>");
            $media = $this->parse_data($row,"<Verified>","</Verified>");


            if(
                (strtotime(date('Y-m-d', strtotime($datetime))) >= strtotime(date('Y-m-d',strtotime($lastdate))))
                and
                (strtotime(date('H', strtotime($datetime))) >= strtotime(date('H',strtotime($lasttime))))
            ){
                AttendanceLog::firstOrCreate([
                    'attendance_machine_id' => $id,
                    'attendance_no_id' => $pin,
                    'date' => date('Y-m-d',strtotime($datetime)),
                    'time' => date('H:i:s',strtotime($datetime)),
                    'media' => $media,
                    'workcode' => $workcode,
                ]);

                echo "<tr><td>".$pin."</td><td>".$datetime."</td><td>".$media."</td><td>".$workcode."</td><td>".$status."</td></tr>";
                echo "<script>window.scrollTo(0,document.body.scrollHeight);</script>";
            }
        }
        echo "</table>";

        return redirect()->route('get_attendance');
    }

    public function parse_data($data,$p1,$p2){
        $data=" ".$data;
        $hasil="";
        $awal=strpos($data,$p1);
        if($awal!=""){
            $akhir=strpos(strstr($data,$p1),$p2);
            if($akhir!=""){
                $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
            }
        }
        return $hasil;
    }
}
