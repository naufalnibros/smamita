<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = ['student_id', 'date', 'time_log', 'time_in', 'time_out', 'work_interval',
        'code', 'flag_attendance', 'description', 'attendance_time'];

    public function student(){
        return $this->belongsTo(Student::class);
    }
}
